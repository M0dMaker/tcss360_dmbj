/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview;

import peerReview.controller.MainController;

/**
 * A helper class that contains the entry point for the application.
 * 
 * @author Jacob Trimble
 */
public final class Main {

	/**
	 * The entry point for the application.
	 * 
	 * @Jacob Trimble
	 * 
	 * @param args The command-line arguments.
	 */
	public static void main(final String[] args) {
		
		MainController controller = new MainController();
		controller.start();
	}
}

/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that contains data about a user.  This type is immutable.
 * 
 * Preconditions listed on methods need to be adhered to.  No method handles precondition violations.
 * 
 * @author Brandon Fjellman
 * @author Dain Landis
 */
public final class User implements Serializable {

	/** Serial Version number. */
	private static final long serialVersionUID = -1152321716230344087L;

	/** Contains the name of the user. */
	private final String myName;
	/** Contains the papers that this user is reviewing. */
	private final List<Paper> myPapersToReview;
	
	/**
	 * Creates a new User using the given information.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theName The name of the user.
	 */
	public User(final String theName) {
		this.myName = theName;
		myPapersToReview = new ArrayList<Paper>();
	}

	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The name of the user. 
	 */
	public String getName() {
		return myName;
	}
	
	/**
	 * Adds the paper to the collection for review.
	 * 
	 * Precondition:  The Paper cannot be null.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param thePaper The paper to review.
	 */
	public void addToReview(final Paper thePaper) {
		
		myPapersToReview.add(thePaper);
	}
	
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The list of papers to be reviewed 
	 */
	public List<Paper> getReviews() {
		return myPapersToReview;
	}
	
	/** 
	 * @author Jacob Trimble
	 * 
	 * @return The name of the user. 
	 */
	@Override
	public String toString() {
		return myName;
	}
	/** 
	 * @author Jacob Trimble
	 * 
	 * @returns whether the given object equals this object. 
	 */
	@Override
	public boolean equals(final Object theOther) {
		if (theOther != null && theOther.getClass() == this.getClass()) {
			return ((User) theOther).myName.equals(this.myName);
		}
		
		return false;
	}
	/** 
	 * @author Jacob Trimble
	 * 
	 * @return The hash-code for this object. 
	 */
	@Override
	public int hashCode() {
		return myName.hashCode();
	}
	
}

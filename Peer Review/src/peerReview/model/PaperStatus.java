/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.model;

/**
 * Defines the status of a paper.
 * 
 * @author Dain Landis
 * @revision Jacob Trimble
 */
public enum PaperStatus {
	/** The paper is submitted but not decided yet.*/
	Submitted,
	/** The paper has been reviewed and needs a recommendation. */
	Reviewed,
	/** The paper is done reviewing and has a recommendation. */
	Recommended,
	/** The paper is accepted. */
	Accepted,
	/** The paper is rejected. */
	Rejected,
}

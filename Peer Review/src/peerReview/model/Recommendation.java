/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.model;

import java.io.File;

/**
 * A class that contains the recommendations for a paper.
 * 
 * Preconditions must be abided by as no defensive programming has been implemented.
 * 
 * @author Brandon Fjellman
 * @author Dain Landis
 */
public final class Recommendation {
	
	/** The subprogram chair. */
	private final SubProgramChair mySubProgramChair;	
	/** The filename for the paper. */
	private final File myFileName;	
	/** The paper the recommendation is for. */
	private final Paper myPaper;
	
	/**
	 * Creates a new recommendation.
	 * 
	 * PreCondition:  Paper and SubProgram chair cannot be null.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theSubProgramChair the subprogramchair that made the recommendation.
	 * @param thePaper The paper for which the recommendation is for.
	 * @param theFileName The file name of the recommendation.
	 */
	public Recommendation(final SubProgramChair theSubProgramChair, final Paper thePaper, final File theFileName) {
		mySubProgramChair = theSubProgramChair;
		myPaper = thePaper;
		myFileName = theFileName;
	}

	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The subprogram chair. 
	 */
	public SubProgramChair getSubProgramChair() {
		return mySubProgramChair;
	}
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The filename. 
	 * 
	 */
	public File getFileName() {
		return myFileName;
	}
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The paper for the recommendation. 
	 * 
	 */
	public Paper getPaper() {
		return myPaper;
	}
	
	/** 
	 * @author Jacob Trimble
	 * 
	 * @returns whether the given object equals this object. 
	 */
	@Override
	public boolean equals(final Object theOther) {
		if (theOther != null && theOther.getClass() == this.getClass()) {
			return ((Recommendation) theOther).myFileName.equals(this.myFileName);
		}
		
		return false;
	}
	/** 
	 * @author Jacob Trimble
	 * 
	 * @return The hash-code for this object. 
	 */
	@Override
	public int hashCode() {
		return myFileName.hashCode();
	}
}

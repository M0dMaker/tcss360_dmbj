package peerReview.model;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

/**
 * The SubProgramChair class holds a list of reviewers and papers assigned to it.
 * 
 * Preconditions must be abided by as no defensive programming has been implemented.
 * 
 * @author Brandon Fjellman
 * @author Dain Landis
 */
public final class SubProgramChair implements Serializable {
	
	/** Serial Version number. */
	private static final long serialVersionUID = 4556606272748448368L;
	/** A special sub-program chair that means that a sub-program chair is not assigned. */
	public static final SubProgramChair NOT_SELECTED = new SubProgramChair(new User("Not Selected"));

	/** The user assigned as a subprogram chair. */
	private final User myUser;
	/** The list of reviewers */
	private final List<User> myReviewers;
	/** The list of papers assigned */
	private final List<Paper> myPapers;
	
	/**
	 * Creates a SubProgramChair object.
	 * 
	 * Precondition:  The User cannot be null.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theUser The user to assign as a subprogram chair.
	 */
	public SubProgramChair(final User theUser) {
		myUser = theUser;
		this.myReviewers = new ArrayList<User>();
		this.myPapers = new ArrayList<Paper>();
	}

	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The user that is this SubProgram Chair. 
	 */
	public User getUser() {
		return myUser;
	}

	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The list of reviewers assigned. 
	 * 
	 */
	public List<User> getReviewers() {
		return myReviewers;
	}

	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The list of papers assigned. 
	 */
	public List<Paper> getPapers() {
		return myPapers;
	}
	
	/**
	 * Add a paper to be reviewed by this SubProgramChair.
	 * 
	 * Precondition:  The Paper cannot be null.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param thePaper The paper to be reviewed.
	 */
	public void addPaper(final Paper thePaper) {
		myPapers.add(thePaper);
	}
	
	/**
	 * Add a reviewer to be able to review for this subprogram chair.
	 * 
	 * Precondition:  The user cannot be null.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theUser User to be added as a reviewer.
	 */
	public void addReviewer(final User theUser) {
		myReviewers.add(theUser);
	}
	
	/** 
	 * @author Jacob Trimble
	 * 
	 * @return The sub-program chair name. 
	 * 
	 */
	@Override
	public String toString() {
		return myUser.getName();
	}
	/** @returns whether the given object equals this object. */
	@Override
	public boolean equals(final Object theOther) {
		if (theOther != null && theOther.getClass() == this.getClass()) {
			return ((SubProgramChair) theOther).myUser.equals(this.myUser);
		}
		
		return false;
	}
	/** 
	 * @author Jacob Trimble
	 * 
	 * @return The hash-code for this object. 
	 */
	@Override
	public int hashCode() {
		return myUser.hashCode();
	}

}

/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that holds data for a single paper.
 * 
 * Preconditions must be abided by as no defensive programming has been implemented.
 * 
 * @author Brandon Fjellman
 * @author Dain Landis
 */
public final class Paper implements Serializable {
	
	/** The serializer version number. */
	private static final long serialVersionUID = 4634349228971312350L;
	
	/** The author of the paper. */
	private final User myAuthor;
	/** The current collection of reviewers. */
	private final List<Review> myReviews;
	/** Contains the conference this paper is for. */
	private final Conference myConference;
	/** The file name of the paper. */
	private File myFileName;
	/** The title of the paper. */
	private String myTitle;
	/** The SPC of the paper */
	private User mySubProgramChair;
	/** Contains the status of the paper. */
	private PaperStatus myStatus;
	
	/**
	 * Creates a new paper using the given data.
	 * 
	 * Precondition:  The all values cannot be null.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * @revision Jacob Trimble
	 * 
	 * 
	 * @param theConference The conference for the paper.
	 * @param theTitle The title of the paper.
	 * @param theAuthor The author of the paper.
	 * @param theFileName The file name of the paper.
	 */
	public Paper(final Conference theConference, final User theAuthor, final String theTitle, final File theFileName) {
		this.myConference = theConference;
		this.myTitle = theTitle;
		this.myFileName = theFileName;
		this.myAuthor = theAuthor;
		this.myStatus = PaperStatus.Submitted;
		this.myReviews = new ArrayList<Review>();
		this.mySubProgramChair = null;
	}
	
	/**
	 * Adds a review to the paper.
	 * 
	 * Precondition:  The Review cannot be null.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theReview The review to add.
	 */
	public void submitReview(final Review theReview) {
		myReviews.add(theReview);
	}
	
	/**
	 * Sets the status of the paper.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theStatus The status of the paper.
	 */
	public void setStatus(final PaperStatus theStatus) {
		myStatus = theStatus;
	}
	
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The file name of the paper. 
	 */
	public File getFileName() {
		return myFileName;
	}
	
	/** 
	 * @author Jacob Trimble
	 * 
	 * @return The conference of the paper. 
	 */
	public Conference getConference() {
		return myConference;
	}
	
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The title of the paper. 
	 */
	public String getTitle() {
		return myTitle;
	}
	
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The author of the paper. 
	 */
	public User getAuthor() {
		return myAuthor;
	}
	
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The reviews for the paper. 
	 */
	public List<Review> getReviews() {
		return myReviews;
	}
	
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The status of the paper. 
	 */
	public PaperStatus getStatus() {
		return myStatus;
	}
		
	/** 
	 * Returns the subprogram chair assigned to this paper 
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 */
	public User getSubProgramChair() {
		return mySubProgramChair;
	}
	
	/** 
	 * Sets the subprogram chair for this paper 
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 */
	public void setSubProgramChair(final User theUser) {
		mySubProgramChair = theUser;
	}
	
	/** 
	 * @author Jacob Trimble
	 * 
	 * @return The title of the paper. 
	 */
	@Override
	public String toString() {
		return myTitle;
	}
	/** 
	 * @author Jacob Trimble
	 * 
	 * @returns whether the given object equals this object. 
	 */
	@Override
	public boolean equals(final Object theOther) {
		if (theOther != null && theOther.getClass() == this.getClass()) {
			final Paper paper = (Paper) theOther;
			return paper.myTitle.equals(this.myTitle) && paper.myConference.equals(this.myConference);
		}
		
		return false;
	}
	/** @author Jacob Trimble
	 * 
	 * @return The hash-code for this object. 
	 */
	@Override
	public int hashCode() {
		return myTitle.hashCode() ^ myConference.hashCode();
	}
	/** 
	 * Sets the title.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theTitle The title to change to.
	 */
	public void setTitle(String theTitle) {
		myTitle = theTitle;
	}
	/**
	 * Sets the File.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theFile The File to change to.
	 */
	public void setFile(File theFile) {
		myFileName = theFile;
		
	}

}

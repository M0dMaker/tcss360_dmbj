/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.model;

import java.io.File;
import java.io.Serializable;

/**
 * A class that contains a single review made by a reviewer. This
 * type is immutable.
 * 
 * Preconditions listed on methods need to be adhered to.  No method handles precondition violations.
 * 
 * @author Brandon Fjellman
 * @author Dain Landis
 */
public final class Review implements Serializable {

	/** Serial Version number. */
	private static final long serialVersionUID = 8468178999175115429L;
	
	/** Contains the reviewer that made the review. */
	private final User myReviewer;
	/** Contains the file name of the review. */
	private File myFileName;
	/** Contains a numerical value rating for the Review */
	private int myRating;
	
	/**
	 * Creates a new review from the given data.
	 * 
	 * Precondition: The User cannot be null.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theUser The user that made the review.
	 * @param theFileName The file name of the review.
	 */
	public Review(final User theUser, final File theFileName, final int theRating) {
		this.myReviewer = theUser;
		this.myFileName = theFileName;
		this.myRating = theRating;
	}
	
	/** 
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The reviewer that made the review. 
	 */
	public User getReviewer() {
		return myReviewer;
	}
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The file name of the review file. 
	 * 
	 */
	public File getFileName() {
		return myFileName;
	}
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The rating of the review. 
	 */
	public int getRating() {
		return myRating;
	}
	
	/** 
	 * Sets the rating for the review from 1 to 5.
	 * 
	 * Will not set value if lower than 1 or greater than 5.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theRating The new rating for the review.
	 */
	public void setRating(final int theRating) {		
		if(theRating > 0 && theRating < 6) {
			this.myRating = theRating;			
		}
	}
	/**
	 * Sets the file name of the review.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theFileName The file name to set to.
	 */
	public void setFileName(final File theFileName) {
		this.myFileName = theFileName;
	}
	
	/** 
	 * @author Jacob Trimble
	 * 
	 * @returns whether the given object equals this object. 
	 * 
	 */
	@Override
	public boolean equals(final Object theOther) {
		if (theOther != null && theOther.getClass() == this.getClass()) {
			return ((Review) theOther).myFileName.equals(this.myFileName);
		}
		
		return false;
	}
	/** 
	 * @author Jacob Trimble
	 * 
	 * @return The hash-code for this object.
	 */
	@Override
	public int hashCode() {
		return myFileName.hashCode();
	}
	
}
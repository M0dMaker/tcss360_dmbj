/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A class that holds the data for a single conference.  This class also acts
 * as the Program Chair.
 * 
 * Preconditions must be abided by as no defensive programming has been implemented.
 * 
 * @author Brandon Fjellman
 * @author Dain Landis
 */
public final class Conference implements Serializable {

	/** The serializer version number. */
	private static final long serialVersionUID = 639734443820745112L;

	/** Contains the papers for the conference. */
	private final List<Paper> myPapers;
	/** Contains a list of the subprogram chairs */
	private final List<SubProgramChair> mySubProgramChairs;
	/** Contains a list of the recommendations. */
	private final List<Recommendation> myRecommendations;
	/** The user who will be the program chair */
	private final User myProgramChair;
	/** Conference name */
	private final String myName;
	/** The last day Papers may be submitted */
	private final Date mySubmissionDate;
	/** The last day Reviews may be submitted */
	private final Date myReviewDate;
	
	/** 
	 * Creates a new Conference.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * Preconditions:  Parameters must not be null.
	 * 
	 * @param theName The name of the conference.
	 * @param theProgramChair The program chair for the conference.
	 * @param theSubmitDate The deadline for the paper submissions.
	 * @param theReviewDate The deadline for review submissions.
	 */
	public Conference(final String theName, final User theProgramChair, final Date theSubmitDate, final Date theReviewDate) {
		this.myPapers = new ArrayList<Paper>();
		this.mySubProgramChairs = new ArrayList<SubProgramChair>();
		this.myRecommendations = new ArrayList<Recommendation>();
		this.myName = theName;
		this.myProgramChair = theProgramChair;
		this.mySubmissionDate = theSubmitDate;
		this.myReviewDate = theReviewDate;
	}
	
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return Returns a list of the subprogram chairs 
	 */
	public List<SubProgramChair> getSubProgramChairs() {
		return mySubProgramChairs;
	}
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return Returns the User who is the Program Chair  
	 */
	public User getProgramChair() {
		return myProgramChair;
	}
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The name of the conference 
	 */
	public String getName() {
		return myName;
	}
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The last day Papers may be submitted 
	 */
	public Date getSubmissionDate() {
		return mySubmissionDate;
	}
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return The last day Reviews may be submitted 
	 */
	public Date getReviewDate() {
		return myReviewDate;
	}
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return Gets the papers submitted to the conference. 
	 */
	public List<Paper> getSubmittedPapers() {
		return myPapers;
	}
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return Gets the recommendations submitted by SubProgram Chairs 
	 */
	public List<Recommendation> getRecommendations() {
		return myRecommendations;
	}
	
	/**
	 * Gets the roles that the given user has in this conference.
	 * 
 	 * Precondition: theUser cannot be null.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * @revision Jacob Trimble
	 * 
	 * @param theUser The user to check.
	 * @return A list of roles the user has.
	 */
	public List<String> getRoles(final User theUser) {
		final List<String> myRoles = new ArrayList<String>();
		
		// Check if Author
		myRoles.add("Author");
		
		// Check if Program Chair
		if (myProgramChair.equals(theUser)) {
			myRoles.add("Program Chair");
		}
		
		// Check if SubProgramChair
		for(final SubProgramChair s : mySubProgramChairs) {
			if (s.getUser().equals(theUser)) {
				myRoles.add("SubProgram Chair");
				break;
			}
		}
		
		// Check if Reviewer
		for (final Paper p : theUser.getReviews()) {
			if (p.getConference().equals(this)) {
				myRoles.add("Reviewer");
				break;
			}
		}
		
		return myRoles;
	}
	
	/**
	 * Adds a sub-program chair to the conference.
	 * 
	 * Precondition: theSPC cannot be null.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theRecommendation The sub-program chair to add.
	 */
	public void addSubProgramChair(final SubProgramChair theSPC) {
		mySubProgramChairs.add(theSPC);
	}
	/**
	 * Adds a paper to the conference.
	 * 
	 * Precondition:  The paper cannot be null.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param thePaper The paper to add.
	 */
	public void submitPaper(final Paper thePaper) {
		myPapers.add(thePaper);
	}
	/**
	 * Submits a recommendation to the conference.
	 * 
	 * Precondition: theRecommendation cannot be null.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theRecommendation The recommendation to add.
	 */
	public void submitRecommendation(final Recommendation theRecommendation) {
		myRecommendations.add(theRecommendation);		
	}
	
	/** 
	 * @author Jacob Trimble
	 * 
	 * @return The title of the conference. 
	 */
	@Override
	public String toString() {
		return myName;
	}
	/** 
	 * @author Jacob Trimble
	 * 
	 * @returns whether the given object equals this object. 
	 */
	@Override
	public boolean equals(final Object theOther) {
		if (theOther != null && theOther.getClass() == this.getClass()) {
			return ((Conference) theOther).myName.equals(this.myName);
		}
		
		return false;
	}
	/** 
	 * @author Jacob Trimble
	 * 
	 * @return The hash-code for this object. 
	 */
	@Override
	public int hashCode() {
		return myName.hashCode();
	}
}

/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.controller;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.PaperStatus;
import peerReview.model.Recommendation;
import peerReview.model.Review;
import peerReview.model.SubProgramChair;
import peerReview.model.User;
import peerReview.view.*;

/**
 * Controls the current state, updates the current state, and chooses
 * which view to show.
 * 
 * @author Team DBMj
 */
public final class MainController 
	implements ReviewListener, AuthorListener, ILoginListener, IChooseConferenceListener,
	IMainFrameListener, ProgramChairListener, SubProgramChairListener  {
	
	/** The file name of the Users file. */
	private static final String myUserFileName = "Users.dat";
	/** The file name of the Conference file. */
	private static final String myConferenceFileName = "Confs.dat";
	
	/** Contains the current users. */
	private final Collection<User> myUsers;
	/** Contains the all conferences. */
	private final Collection<Conference> myConferences;
	/** Contains the main window and frame. */
	private final MainFrame myFrame;
	/** Contains the current logged-in User. */
	private User myCurrentUser;
	/** Contains the current conference. */
	private Conference myConference;
	/** Contains the current view. */
	private IView myView;
	
	/** Creates a new MainController object. */
	public MainController() {
		this.myFrame = new MainFrame(this);		
		this.myUsers = Serializer.loadUsers(myUserFileName);		
		this.myConferences = Serializer.loadConferences(myConferenceFileName);

		addDefaultData();
		
		if (!new File("Example.txt").exists()) {
			try {
				new File("Example.txt").createNewFile();
			} catch (final IOException e) { }
		}
	}

	/**
	 * Logs in the given user.
	 * 
	 * @author Brandon Fjellman
	 * @revision Dain Landis
	 * 
	 * @param loginName The name of the user to login.
	 * @return Whether the user was found.
	 */
	@Override
	public boolean login(final String loginName) {
		myCurrentUser = findUser(loginName);
		if (myCurrentUser != null) {
			
			// Show the next view.
			myView = new ChooseConferenceView(this);
			myView.show(myFrame);
			
			return true;
		}
		
		return false;
	}
	/**
	 * Submits a review for the given paper.
	 * 
	 * @author Dain Landis
	 * @revision Brandon Fjellman
	 * @param thePaper The paper to submit the paper to.
	 * @param theFileName The file name of the review.
	 */
	@Override
	public void submitReview(final Paper thePaper, final File theFileName, final int theRating) {
		final Review myReview = new Review(myCurrentUser, theFileName, theRating);
		final List<Paper> papers = myConference.getSubmittedPapers();
		
		for (final Paper p : papers) {
			if (p.equals(thePaper)) {
				boolean found = false;
				for (final Review r : p.getReviews()) {
					if (r.getReviewer().equals(myCurrentUser)) {
						r.setRating(theRating);
						r.setFileName(theFileName);
						found = true;
					}
				}
				
				if (!found) {
					p.submitReview(myReview);					
				}
			}
		}
		
		updatePaperStatus(thePaper);
	}
	
	/**
	 * Submits a paper to the conference.
	 * US3, BR4
	 * Checks if a paper is already in a conference.
	 * 
	 * @author Mike Dean
	 * @revision Dain Landis
	 * @revision Brandon Fjellman
	 * 
	 * @param theFileName the file name of the paper
	 * @param theTitle the title of the paper
	 * @return false if paper is already submitted true if not.
	 */
	@Override
	public boolean submitPaper(final File theFileName, final String theTitle) {
		for(final Paper p : myConference.getSubmittedPapers()) {
			if(p.getTitle().equals(theTitle)) {
				return false;
			}
		}
		
		final Paper myPaper = new Paper(myConference, myCurrentUser, theTitle, theFileName);
		myConference.submitPaper(myPaper);
		return true;
	}
	
	/** 
	 * @author Jacob Trimble
	 * 
	 * @return Whether the user can submit a paper to the current conference. 
	 */
	@Override
	public boolean canSubmitPaper() {
		return myConference.getSubmissionDate().after(new Date());
	}
	/**
	 * Sets the decision for the given paper.
	 * 
	 * @author Jacob Trimble
	 * @param theDecision The string decision to set to.
	 * @param thePaper The paper to set for.
	 */
	@Override
	public void setDecision(final PaperStatus theDecision, final Paper thePaper) {
		if (thePaper.getStatus() == PaperStatus.Recommended && 
			(theDecision == PaperStatus.Accepted || theDecision == PaperStatus.Rejected)) {
				thePaper.setStatus(theDecision);
		}
	}
	/**
	 * Gets the papers of the current signed-in user.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 */
	@Override
	public List<Paper> getUserPapers() {
		final List<Paper> myUsersPapers = new ArrayList<Paper>();
			
		for (final Paper p : myConference.getSubmittedPapers()) {
			if (p.getAuthor().equals(myCurrentUser)) {
				myUsersPapers.add(p);
			}
		}
			
		return myUsersPapers;
	}
	/**
	 * Sets the current conference.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param theConference The conference to set to.
	 */
	@Override
	public void chooseConference(final Conference theConference) {
		myConference = theConference;
		
		myView = new AuthorView(this);
		myView.show(myFrame);
	}
	/** 
	 * @author Jacob Trimble 
	 * 
	 * @return The current conferences.
	 */
	@Override
	public Collection<Conference> getConferences() {
		return myConferences;
	}
	/** 
	 * Called when the program closes. 
	 * 
	 * @author Jacob Trimble
	 */
	@Override
	public void close() {		
		Serializer.saveConferences(myConferenceFileName, myConferences);
		Serializer.saveUsers(myUserFileName, myUsers);		
	}
	/**
	 * Logs out the current user and shows the login screen. 
	 * 
	 * @author Jacob Trimble 
	 */
	@Override
	public void logout() {
		myCurrentUser = null;
		myConference = null;
		
		myView = new LoginView(this);
		myView.show(myFrame);
	}
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return Gets the current user. 
	 */
	@Override
	public User getCurrentUser() {
		return myCurrentUser;
	}
	/** 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 *  
	 * @return Gets the current conference. 
	 */
	@Override
	public Conference getConference() {
		return myConference;
	}
	
	/**
	 * Starts the controller and shows the first view.
	 * 
	 * @author Jacob Trimble
	 * @revision Mike Dean
	 */
	public void start() {
		myView = new LoginView(this);
		myView.show(myFrame);
		
		myFrame.show();
	}
	
	/** Sets the view to the program chair view. 
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis 
	 */
	@Override
	public void setViewProgramChair() {
		myView = new ProgramChairView(this);
		myView.show(myFrame);
	}
	/** Sets the view to the author view. 
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 */	
	@Override
	public void setViewSubProgramChair() {
		myView = new SubProgramChairView(this, myConference, myUsers);
		myView.show(myFrame);
		
	}
	/** Sets the view to the author view. 
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 */
	@Override
	public void setViewAuthor() {
		myView = new AuthorView(this);
		myView.show(myFrame);
	}
	/** Sets the view to the review view. 
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis 
	 */
	@Override
	public void setViewReview() {
		myView = new ReviewView(this);
		myView.show(myFrame);
	}
	
	/**
	 * US6 Designate a Subprogram Chair
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theUser the user to set as spc. 
	 */
	@Override
	public void setSubPC(final User theUser) {
		final SubProgramChair spc = new SubProgramChair(theUser);
		myConference.addSubProgramChair(spc);
	}
	
	/**
	 * Assigns a paper to the subprogram chair.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return Returns an integer value:  0 if pass, 1 if fail because SPC is author, 
	 *  2 if > than 3 assigned, -1 on error.
	 */
	@Override
	public int addPaperToSubPC(final Paper thePaper, final SubProgramChair theSubPC) {
		if (thePaper.getSubProgramChair() != null) {
			return -1;
		}
		
		if (theSubPC.getUser().equals(thePaper.getAuthor())) {
			return 1;
		}
		
		if (theSubPC.getPapers().size() > 3) {
			return 2;
		}
		
		theSubPC.addPaper(thePaper);
		thePaper.setSubProgramChair(theSubPC.getUser());
		return 0;
	}
	
	/**
	 * Assign a paper to a user.  
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param thePaper The paper to assign.
	 * @param theReviewer The user that reviews the paper.
	 * @return Returns an integer value:  0 if pass, 1 if fail because user is author, 
	 * 2 if > than 3 assigned.
	 */
	@Override
	public int assignPaper(final Paper thePaper, final User theReviewer) {
		if (theReviewer.equals(thePaper.getAuthor())) {
			return 1;
		}
		
		if (getReviews(theReviewer).size() > 3) {
			return 2;
		}
		
		if (!theReviewer.getReviews().contains(thePaper)) {
			theReviewer.addToReview(thePaper);
			updatePaperStatus(thePaper);
		}
		return 0;
	}

	/**
	 * Sub PC submit recommendation and set recommended on paper
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param theSubProgramChair the SubPC making the recommendation  
	 * @param thePaper the paper that the recommendation is being set on.
	 * @param theFileName the filename of the paper.
	 */
	@Override
	public void submitRecommentation(final SubProgramChair theSubProgramChair, final Paper thePaper, final File theFileName) {
		myConference.submitRecommendation(new Recommendation(theSubProgramChair, thePaper, theFileName));	
		updatePaperStatus(thePaper);
	}

	/**
	 * Creates a list of reviewers for a given paper
	 * GUI field population.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param thePaper thePaper to get reviewers from.
	 * @return returns a list of reviewers.
	 */
	@Override
	public List<User> getReviewers(final Paper thePaper) {
		final List<User> myUserList = new ArrayList<User>();
		for(final User user : myUsers) {
			if (user.getReviews().contains(thePaper)) {
				myUserList.add(user);
			}
		}
		
		return myUserList;
	}
	
	/**
	 * Gets a list of the current papers that the reviewer is reviewing.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @return The list of papers.
	 */
	@Override
	public List<Paper> getReviews(final User theUser) {
		final List<Paper> myReviews = new ArrayList<Paper>();
		for(final Paper paper : theUser.getReviews()) {
			if (paper.getConference().equals(myConference)) {
				myReviews.add(paper);
			}
		}
		
		return myReviews;
	}
	
	/**
	 * Checks to see if the review deadline has passed for submitting new reviews.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @return If the review is able to be submitted.
	 */
	@Override
	public boolean canSubmitReview() {
		return myConference.getReviewDate().after(new Date());
	}

	/**
	 * Finds the user with the given user name.
	 * 
	 * @author Jacob Trimble
	 * @param theName The name of the user.
	 * @return The user with that name, or null.
	 */
	private User findUser(final String theName) {
		for (final User user : myUsers) {
			if (theName.equals(user.getName())) {
				return user;
			}
		}
		
		return null;
	}
	/** 
	 * Adds some default data to the controller.
	 * 
	 * @author Jacob Trimble 
	 */
	private void addDefaultData() {
		if (myUsers.size() == 0) {
			myUsers.add(new User("Jacob"));
			myUsers.add(new User("Mike"));
			myUsers.add(new User("Brandon"));
			myUsers.add(new User("Dain"));
			
			
			for (int i = 0; i < 50; ++i) {
				myUsers.add(new User(DataGenerator.getUserName()));
			}
		}
		
		if (myConferences.size() == 0) {
			DataGenerator.fillConferences(myConferences, myUsers);
			
			for (final Conference conf : myConferences) {
				myConference = conf;
				for (final Paper paper : conf.getSubmittedPapers()) {
					updatePaperStatus(paper);
				}
			}
			
			myConference = null;
		}
		
		if (!myUsers.contains(new User("User"))) {
			myUsers.add(new User("User"));
		}
	}
	/**
	 * Gets whether the given paper has all the reviews made.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param thePaper The paper to search for.
	 * @return True if all reviews are in; otherwise false.
	 */
	private boolean hasAllReviews(final Paper thePaper) {
		boolean any = false;
		for (final User u : myUsers) {
			if (u.getReviews().contains(thePaper)) {
				boolean found = false;
				for (final Review r : thePaper.getReviews()) {
					if (r.getReviewer().equals(u)) {
						found = true;
						break;
					}
				}
				
				any = true;
				if (!found)
					return false;
			}
		}
		
		return any;
	}
	/**
	 * Checks whether the given paper has a recommendation.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param thePaper The paper to check.
	 * @return True if the paper has a recommendation; otherwise false.
	 */
	private boolean hasRecommendation(final Paper thePaper) {
		for (final Recommendation r : myConference.getRecommendations()) {
			if (r.getPaper().equals(thePaper)) {
				return true;
			}
		}
		
		return false;
	}
	/**
	 * Updates the status of the given paper.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * @revision Jacob Trimble
	 * 
	 * @param thePaper The paper to change the status of.
	 */
	private void updatePaperStatus(final Paper thePaper) {
		if (thePaper.getStatus() != PaperStatus.Accepted &&
				thePaper.getStatus() != PaperStatus.Rejected) {
			
			if (hasRecommendation(thePaper)) {
				thePaper.setStatus(PaperStatus.Recommended);
			} else if (hasAllReviews(thePaper)) {
				thePaper.setStatus(PaperStatus.Reviewed);
			} else {
				thePaper.setStatus(PaperStatus.Submitted);			
			}
		}
	}
	
	/**
	 * Downloads the given file.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param theFile The file to open.
	 */
	@Override
	public void downloadFile(final File theFile) {
		try {
			Desktop.getDesktop().open(theFile);
		} catch (final Exception e) { }
	}
	/**
	 * Edits the title and filename of a Paper.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * 
	 * @param thePaper The Paper to be edited.
	 * @param theTitle The title to be changed to.
	 * @param theFile The File to be changed to.
	 */
	@Override
	public void editPaper(final Paper thePaper, final String theTitle, final File theFile) {
		for(final Paper p : myConference.getSubmittedPapers()) {
			if(p.equals(thePaper)) {
				p.setTitle(theTitle);
				p.setFile(theFile);
			}
		}
	}
	/**
	 * Removes a Paper from the Conference.
	 * 
	 * @author Brandon Fjellman
	 * @author Dain Landis
	 * @revision Jacob Trimble
	 * 
	 * @param thePaper The paper to remove from the conference.
	 */
	@Override
	public void removePaper(final Paper thePaper) {
		myConference.getSubmittedPapers().remove(thePaper);
		for (final User user : myUsers) {
			user.getReviews().remove(thePaper);
		}
		
		// Remove sub-program chair assignments.
		for (final SubProgramChair spc : myConference.getSubProgramChairs()) {
			spc.getPapers().remove(thePaper);
		}
		
		// Remove recommendations.
		for (final Recommendation rec : myConference.getRecommendations()) {
			if (rec.getPaper().equals(thePaper)) {
				myConference.getRecommendations().remove(rec);
				break;
			}
		}
	}
	
}

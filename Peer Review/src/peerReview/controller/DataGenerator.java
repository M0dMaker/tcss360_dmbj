/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.Recommendation;
import peerReview.model.Review;
import peerReview.model.SubProgramChair;
import peerReview.model.User;

/**
 * Contains helper methods to generate random conference data for
 * first startup.
 * 
 * @author Jacob Trimble
 */
public final class DataGenerator {
	
	/** Contains a static random number generator. */
	private static final Random myRand = new Random();
	/** Contains the names that have been used. */
	private static final Set<String> myNameUsed = new HashSet<String>();
	
	/** Contains the titles of papers. */
	private static final List<String> myTitles;
	/** Contains the current index in myTitles. */
	private static int myTitleIndex = 0;
	
	/** Contains the titles of conferences. */
	private static final List<String> myConfs;
	/** Contains the current index in myConfs. */
	private static int myConfsIndex = 0;
	
	static {
		myTitles = Arrays.asList(
				"Monopolistic competition and optimum product diversity",
				"Ecological diversity and its measurement",
				"A simple transmit diversity technique for wireless communications",
				"Measuring biological diversity",
				"Some computer science issues in ubiquitous computing",
				"A theory of timed automata",
				"Calibration of the Computer Science and Applications, Inc. accelerometer",
				"Computer science as empirical inquiry: Symbols and search",
				"Understanding power quality problems",
				"Noninvasive diagnosis of deep venous thrombosis",
				"Signal processing of power quality disturbances",
				"Nucleotide sequence and gene organization of sea urchin mitochondrial DNA",
				"Agricultural intensification and ecosystem properties",
				"A problem from hell: America and the age of genocide",
				"Challenges in the quest for keystones",
				"Childhood predictors of adult obesity: a systematic review",
				"Acculturation attitudes in plural societies",
				"The bases of social power",
				"Lawrence Kohlberg's approach to moral education",
				"A comprehensive grammar of the English language",
				"Research methods in education",
				"Qualitative research in education. An introduction to theory and methods",
				"Organizational theory, design, and change",
				"Criminal behavior: A psychosocial approach",
				"Case study research in education: A qualitative approach",
				"Highly-complex malware has secretly spied on computers for years, say researchers",
				"Why Windows 10 isn�t version 6 any more and why it will probably work",
				"The first enemy of C++ is its past",
				"The 14 mistakes that will kill your independent development shop",
				"I Hope this works..."
			);
		
		myConfs = Arrays.asList(
				"Connectivity Online STEAM Conference",
				"International Conference on Improving Sustainability Concept In Developing Countries",
				"Second International Conference of Information Science and Techniques",
				"The 8th Dubai International Conference for Medical Sciences",
				"4th World Congress of Regional Anaesthesia and Pain Therapy",
				"The 2nd World Congress on Clinical Lipidology",
				"International Conference on Web Open Access to Learning",
				"Sixth International Conference on Network Communications Security",
				"Fifth International Conference on Computer Science, Engineering and Applications",
				"New Perspectives In Construction Law International Scientific Conference",
				"International Academic Conference on law and Politics",
				"The Annual Conference of the international Association of Law and Forensic Science",
				"The Tel Aviv Institutional Investment Conference",
				"The Supernatural in Literature and Film",
				"I can't think of anything"
				);
		
		shuffle();
		Collections.shuffle(myConfs);
	}

	/** @return A random user name. 
	 * 
	 *  @author Jacob Trimble
	 */
	public synchronized static String getUserName() {
		String ret;
		do {
			String first = getFirstName().toLowerCase();
			String last = getLastName().toLowerCase();
			first = Character.toUpperCase(first.charAt(0)) + first.substring(1);
			last = Character.toUpperCase(last.charAt(0)) + last.substring(1);
			
			ret = first + " " + last;
		} while (myNameUsed.contains(ret));
		
		myNameUsed.add(ret);
		return ret;
	}

	/**
	 * Gets a random index in the range [0, max).
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param theMax The exclusive maximum number.
	 * @return A random number in the range [0, max).
	 */
	public synchronized static int getIndex(final int theMax) {
		return myRand.nextInt(theMax);
	}
	
	/**
	 * Fills the given conferences collection with random conferences.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param theConferences The collection to fill.
	 * @param theUsers The current users.
	 */
	public synchronized static void fillConferences(final Collection<Conference> theConferences, 
			final Collection<User> theUsers) {
		
		final User jacob = findUser(theUsers, "Jacob");
		final User dain = findUser(theUsers, "Dain");
		final User mike = findUser(theUsers, "Mike");
		final User brandon = findUser(theUsers, "Brandon");

		shuffle();
		myConfsIndex = 0;

		// Create a conference in the past.
		final Conference c1 = createConference(jacob, theUsers,
				DataGenerator.getPastDate(), DataGenerator.getPastDate());
		theConferences.add(c1);	

		shuffle();
		
		// Create a conference around now.
		final Conference c2 = createConference(dain, theUsers,
				DataGenerator.getPastDate(), DataGenerator.getFutureDate());
		theConferences.add(c2);

		shuffle();
		
		// Create a conference in the future.
		final Conference c3 = createConference(mike, theUsers,
				DataGenerator.getFutureDate(), DataGenerator.getFutureDate());
		theConferences.add(c3);

		shuffle();
		
		// Create a conference in the future.
		final Conference c4 = createConference(brandon, theUsers,
				DataGenerator.getFutureDate(), DataGenerator.getFutureDate());
		theConferences.add(c4);
	}
	
	/** Shuffles the current data.
	 *
	 *  @author Jacob Trimble
	 * 
	 */
	private static void shuffle() {
		Collections.shuffle(myTitles);
		myTitleIndex = 0;
	}
	
	/** 
	 * @author Jacob Trimble
	 * @return A random date in the future. 
	 * 
	 */
	private static Date getFutureDate() {
		return new GregorianCalendar(2015 + getIndex(3), getIndex(12), getIndex(28)).getTime();		
	}
	
	/** 
	 * @author Jacob Trimble
	 * @return A random date in the past. 
	 */
	private static Date getPastDate() {
		return new GregorianCalendar(2011 + getIndex(3), getIndex(12), getIndex(28)).getTime();
	}
	
	/** 
	 * @author Jacob Trimble
	 * @return A random paper title. 
	 * 
	 */
	private static String getTitle() {
		return myTitles.get(myTitleIndex++);
	}
	
	/** 
	 * @author Jacob Trimble
	 * @return A random conference title. 
	 */
	private static String getConference() {
		return myConfs.get(myConfsIndex++);
	}
	
	/**
	 * Creates a new random conference.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param thePC The program chair for the conference.
	 * @param theUsers The current users.
	 * @param theSubmitDate The submition deadline.
	 * @param theReviewDate The review deadline.
	 * @return A new random conference.
	 */
	private static Conference createConference(final User thePC, final Collection<User> theUsers,
			final Date theSubmitDate, final Date theReviewDate) {

		final User jacob = findUser(theUsers, "Jacob");
		final User dain = findUser(theUsers, "Dain");
		final User mike = findUser(theUsers, "Mike");
		final User brandon = findUser(theUsers, "Brandon");
		final User[] admins = { jacob, dain, mike, brandon };
		
		final Conference ret = new Conference(
				DataGenerator.getConference(), thePC, 
				theSubmitDate, theReviewDate);
		
		// Add some sub-program chairs.
		ret.addSubProgramChair(new SubProgramChair(jacob));
		ret.addSubProgramChair(new SubProgramChair(dain));
		ret.addSubProgramChair(new SubProgramChair(mike));
		ret.addSubProgramChair(new SubProgramChair(brandon));		
		for (int i = 0; i < 3; i++) {
			User user;
			do {
				user = randomUser(theUsers);
			} while (user.equals(jacob) || user.equals(mike) || user.equals(brandon) || user.equals(dain));

			ret.addSubProgramChair(new SubProgramChair(user));
		}
		
		// Add some papers for the admins.
		for (final User u : admins) {
			final int max = getIndex(3) + 1;
			for (int i = 0; i < max; ++i) {
				final Paper p = getPaper(ret, theUsers, u);
				ret.submitPaper(p);
			}
		}
		
		// Add some more papers.
		while (myTitleIndex < myTitles.size()) {
			final User author = randomUser(theUsers);
			final Paper p = getPaper(ret, theUsers, author);
			ret.submitPaper(p);
		}
		
		return ret;
	}
	
	/**
	 * Creates a random paper for the given user.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param theUser The author of the paper.
	 * @return A new random paper.
	 */
	private static Paper getPaper(final Conference theConf, final Collection<User> theUsers, final User theUser) {
		final List<SubProgramChair> spcs = theConf.getSubProgramChairs();
		final String title = getTitle();
		final Paper ret = new Paper(theConf, theUser, title, new File("Example.txt"));
		
		if (getIndex(10) < 6) {
			SubProgramChair spc;
			do {
				spc = spcs.get(getIndex(spcs.size()));
			} while (spc.getUser().equals(theUser));
			spc.addPaper(ret);
			ret.setSubProgramChair(spc.getUser());
			
			// Add some reviewers.
			final List<User> reviewers = new ArrayList<User>();
			User reviewer = null;
			for (int i = 0; i < 4; i++) {
				reviewer = randomUser(theUsers);
				if (!reviewer.equals(theUser) && getReviewCount(reviewer, theConf) < 4) {
					reviewer.addToReview(ret);
					reviewers.add(reviewer);
				}
			}
			
			// Add a review.
			final Review review = new Review(reviewer, new File("Example.txt"), getIndex(4) + 1);
			ret.submitReview(review);

			if (getIndex(10) < 4) {
				for (final User rev : reviewers) {
					if (!review.getReviewer().equals(rev)) {
						final Review temp = new Review(rev, new File("Example.txt"), getIndex(4) + 1);
						ret.submitReview(temp);
					}
				}
				
				if (getIndex(10) < 2) {
					// Add a recommendation.
					final Recommendation rec = new Recommendation(spc, ret, new File("Example.txt"));
					theConf.submitRecommendation(rec);
				}
			}
		}
		return ret;
	}

	/**
	 * Finds the user with the given user name.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param theName The name of the user.
	 * @return The user with that name, or null.
	 */
 	private static User findUser(final Collection<User> theUsers, final String theName) {
		for (final User user : theUsers) {
			if (theName.equals(user.getName())) {
				return user;
			}
		}
		
		return null;
	}
	
	/** @return A random user.
	 *  @author Jacob Trimble 
	 */
	private static User randomUser(final Collection<User> theUsers) {
		int i = DataGenerator.getIndex(theUsers.size());
		for (final User u : theUsers) {
			if (i-- <= 0) {
				return u;
			}
		}
		return null;
	}
	
	/** @return A random first name. 
	 *  @author Jacob Trimble 
	 */
	private static String getFirstName() {
		final String[] data = 
			{ 
				"JAMES", "JOHN", "ROBERT", "MICHAEL", "WILLIAM", "DAVID", "RICHARD", "CHARLES", "JOSEPH", 
				"THOMAS", "CHRISTOPHER", "DANIEL", "PAUL", "MARK", "DONALD", "GEORGE", "TOMMY", "LEON", 
				"DEREK", "WARREN", "DARRELL", "JEROME", "FLOYD", "LEO", "ALVIN", "TIM", "WESLEY", 
				"GORDON", "DEAN", "GREG", "JORGE", "DUSTIN", "PEDRO", "DERRICK", "DAN", "LEWIS", "ZACHARY", 
				"COREY", "HERMAN", "MAURICE", "VERNON", "ROBERTO", "CLYDE", "GLEN", "HECTOR", "SHANE", 
				"RICARDO", "SAM", "RICK", "LESTER", "BRENT", "RAMON", "CHARLIE", "TYLER", "GILBERT",
				"GENE", "MARC", "REGINALD", "RUBEN", "BRETT", "ANGEL", "NATHANIEL", "RAFAEL", "LESLIE", 
				"EDGAR", "MILTON", "RAUL", "BEN", "CHESTER", "CECIL", "DUANE", "FRANKLIN", "ANDRE", 
				"ELMER", "BRAD", "GABRIEL", "RON", "MITCHELL", "ROLAND", "ARNOLD", "HARVEY", "JARED", 
				"ADRIAN", "KARL", "CORY", "CLAUDE", "ERIK", "DARRYL", "JAMIE",  "NEIL", "JESSIE", 
				"CHRISTIAN", "JAVIER", "FERNANDO", "CLINTON", "TED", "MATHEW", "TYRONE", "DARREN", 
				"LONNIE", "LANCE", "CODY", "JULIO", "KELLY", "KURT", "ALLAN", "NELSON", "GUY", "CLAYTON", 
				"HUGH", "MAX", "DWAYNE", "DWIGHT", "ARMANDO", "FELIX", "JIMMIE", "EVERETT", "JORDAN", 
				"IAN", "WALLACE", "KEN", "BOB", "JAIME", "CASEY", "ALFREDO", "ALBERTO", "DAVE", "IVAN", 
				"JOHNNIE", "SIDNEY", "BYRON", "JULIAN", "ISAAC", "MORRIS", "CLIFTON", "WILLARD", "DARYL", 
				"ROSS", "VIRGIL", "ANDY", "MARSHALL", "SALVADOR", "PERRY", "KIRK", "SERGIO", "MARION", 
				"TRACY", "SETH", "KENT", "TERRANCE", "RENE", "EDUARDO", "TERRENCE", "ENRIQUE", "FREDDIE", "WADE" 
			};
		
		return data[getIndex(data.length)];
	}

	/** @return A random last name. 
	 * 
	 *  @author Jacob Trimble 
	 */
	private static String getLastName() {
		final String[] data = 
			{ 
				"SMITH", "JOHNSON", "WILLIAMS", "BROWN", "JONES", "MILLER", "DAVIS", "GARCIA", "RODRIGUEZ",
				"WILSON", "MARTINEZ", "ANDERSON", "TAYLOR", "THOMAS", "HERNANDEZ", "MOORE", "MARTIN",
				"JACKSON", "THOMPSON", "WHITE", "LOPEZ", "LEE", "GONZALEZ", "HARRIS", "CLARK", "LEWIS", 
				"ROBINSON", "WALKER", "PEREZ", "HALL", "YOUNG", "ALLEN", "SANCHEZ", "WRIGHT", "KING",
				"SCOTT", "GREEN", "BAKER", "ADAMS", "NELSON", "HILL", "RAMIREZ", "CAMPBELL", "MITCHELL",
				"ROBERTS", "CARTER", "PHILLIPS", "EVANS", "TURNER", "TORRES", "PARKER", "COLLINS", 
				"EDWARDS", "STEWART", "FLORES", "MORRIS", "NGUYEN", "MURPHY", "RIVERA", "COOK", "ROGERS", 
				"MORGAN", "PETERSON", "COOPER", "REED", "BAILEY", "BELL", "GOMEZ", "KELLY", "HOWARD", 
				"WARD", "COX", "DIAZ", "RICHARDSON", "WOOD", "WATSON", "BROOKS", "BENNETT", "GRAY", 
				"JAMES", "REYES", "CRUZ", "HUGHES", "PRICE", "MYERS", "LONG", "FOSTER", "SANDERS",
				"ROSS", "MORALES", "POWELL", "SULLIVAN", "RUSSELL", "ORTIZ", "JENKINS", "GUTIERREZ", 
				"PERRY", "BUTLER", "BARNES", "FISHER", "HENDERSON", "COLEMAN", "SIMMONS", "PATTERSON", 
				"JORDAN", "REYNOLDS", "HAMILTON", "GRAHAM", "KIM", "GONZALES", "ALEXANDER", "RAMOS", 
				"WALLACE", "GRIFFIN", "WEST", "COLE", "HAYES", "CHAVEZ", "GIBSON", "BRYANT", "ELLIS", 
				"STEVENS", "MURRAY", "FORD", "MARSHALL", "OWENS", "MCDONALD", "HARRISON", "RUIZ", 
				"KENNEDY", "WELLS", "ALVAREZ", "WOODS", "MENDOZA", "CASTILLO", "OLSON", "WEBB", 
				"WASHINGTON", "TUCKER", "FREEMAN", "BURNS", "HENRY", "VASQUEZ", "SNYDER", "SIMPSON", 
				"CRAWFORD", "JIMENEZ", "PORTER", "MASON", "SHAW", "GORDON", "WAGNER", "HUNTER", "ROMERO", 
				"HICKS", "DIXON", "HUNT", "PALMER", "ROBERTSON", "BLACK", "HOLMES", "STONE", "MEYER",
				"BOYD", "MILLS", "WARREN", "FOX", "ROSE", "RICE", "MORENO", "SCHMIDT", "PATEL", "FERGUSON",
				"NICHOLS", "HERRERA", "MEDINA", "RYAN", "FERNANDEZ", "WEAVER", "DANIELS", "STEPHENS", 
				"GARDNER", "PAYNE", "KELLEY", "DUNN", "PIERCE", "ARNOLD", "TRAN", "SPENCER", "PETERS",
				"HAWKINS", "GRANT", "HANSEN", "CASTRO", "HOFFMAN", "HART", "ELLIOTT", "CUNNINGHAM", 
				"KNIGHT", "BRADLEY", "CARROLL", "HUDSON", "DUNCAN", "ARMSTRONG", "BERRY", "ANDREWS", 
				"JOHNSTON", "RAY", "LANE", "RILEY", "CARPENTER", "PERKINS", "AGUILAR", "SILVA", "RICHARDS", 
				"WILLIS", "MATTHEWS", "CHAPMAN", "LAWRENCE", "GARZA", "VARGAS", "WATKINS", "WHEELER", 
				"LARSON", "CARLSON", "HARPER", "GEORGE", "GREENE", "BURKE", "GUZMAN", "MORRISON", "MUNOZ",
				"JACOBS", "OBRIEN", "LAWSON", "FRANKLIN", "LYNCH", "BISHOP", "CARR", "SALAZAR", "AUSTIN", 
				"MENDEZ", "GILBERT", "JENSEN", "WILLIAMSON", "MONTGOMERY", "HARVEY", "OLIVER", "HOWELL", 
				"DEAN", "HANSON", "WEBER", "GARRETT", "SIMS", "BURTON", "FULLER", "SOTO", "MCCOY", 
				"WELCH", "CHEN", "SCHULTZ", "WALTERS", "REID", "FIELDS", "WALSH", "LITTLE", "FOWLER",
				"BOWMAN", "DAVIDSON", "MAY", "DAY", "SCHNEIDER", "NEWMAN", "BREWER", "LUCAS", "HOLLAND",
				"WONG", "BANKS", "SANTOS", "CURTIS", "PEARSON", "DELGADO", "VALDEZ", "PENA", "RIOS", 
				"DOUGLAS", "SANDOVAL", "BARRETT", "HOPKINS", "KELLER", "GUERRERO", "STANLEY" };
		
		return data[getIndex(data.length)];
	}
	/**
	 * Gets the count of the reviews.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param theUser The user to count reviews for.
	 * @param theConf The currently selected conference.
	 * @return The amount of reviews.
	 */
	private static int getReviewCount(final User theUser, final Conference theConf) {
		int ret = 0;
		for (final Paper p : theUser.getReviews()) {
			if (p.getConference().equals(theConf)) {
				ret++;
			}
		}
		
		return ret;
	}
}

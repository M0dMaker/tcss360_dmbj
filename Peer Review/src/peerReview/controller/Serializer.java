/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;

import peerReview.model.Conference;
import peerReview.model.User;

/**
 * A helper type that converts to and from a file system.
 * 
 * @author Jacob Trimble
 */
public final class Serializer {

	/**
	 * Saves conferences to the given file stream.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param theFileName The name of the file to save to.
	 * @param theConferences The conference to save to.
	 */
	static void saveConferences(final String theFileName, final Collection<Conference> theConferences) {
		saveObjects(theFileName, theConferences);
	}
	/**
	 * Loads conferences from the given file stream.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param theFileName The name of the file to load from.
	 * @return The conferences that was loaded.
	 */
	static Collection<Conference> loadConferences(final String theFileName) {
		return loadObjects(theFileName);
	}
	
	/**
	 * Saves the users to the user file.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param theFileName The name of the file to save to.
	 * @param theUsers The users to save.
	 */
	static void saveUsers(final String theFileName, final Collection<User> theUsers) {
		saveObjects(theFileName, theUsers);
	}
	/**
	 * Loads the users from the users file.
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param theFileName The name of the file to load from.
	 * @return The users that were loaded.
	 */
	static Collection<User> loadUsers(final String theFileName) {
		return loadObjects(theFileName);
	}
	
	/**
	 * Saves the given objects to the given file. 
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param theFileName The name of the file to save to.
	 * @param theObjects The objects to save.
	 */
	static <T> void saveObjects(final String theFileName, final Collection<T> theObjects) {
		try {
			FileOutputStream file = new FileOutputStream(theFileName);
			ObjectOutputStream out = new ObjectOutputStream(file);
			
			out.writeInt(theObjects.size());
			for (final T c : theObjects) {
				out.writeObject(c);
			}
			
			file.close();
		} catch (IOException e) { }
	}
	/**
	 * Loads the given objects from the given file. 
	 * 
	 * @author Jacob Trimble
	 * 
	 * @param theFileName The name of the file to load from.
	 * @return The objects that were loaded.
	 */
	@SuppressWarnings("unchecked")
	static <T> Collection<T> loadObjects(final String theFileName) {
		Collection<T> ret = new ArrayList<T>();
		try {
			
			FileInputStream file = new FileInputStream(theFileName);
			ObjectInputStream in = new ObjectInputStream(file);

			int count = in.readInt();
			while (count-- > 0) {
				ret.add((T) in.readObject());
			}
			
			file.close();			
			
		} catch (IOException e) { }
		catch (ClassNotFoundException e) { }
		
		return ret;
	}
	
}

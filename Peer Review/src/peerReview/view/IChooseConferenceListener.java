/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.view;

import java.util.Collection;

import peerReview.model.Conference;

/**
 * Interface for choose conference.
 * 
 * @author Mike Dean
 */
public interface IChooseConferenceListener extends IMainFrameListener {

	/**
	 * Changes the current conference in the controller.
	 * 
	 * @param theConference The conference that was chosen.
	 */
	public abstract void chooseConference(final Conference theConference);
	/** @return The current conferences. */
	public abstract Collection<Conference> getConferences();
	
}

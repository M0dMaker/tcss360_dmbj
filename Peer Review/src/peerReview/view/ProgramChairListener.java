package peerReview.view;

import peerReview.model.Paper;
import peerReview.model.PaperStatus;
import peerReview.model.SubProgramChair;
import peerReview.model.User;

/**
 * Listener interface for Program Chair view.
 * 
 * @author Dain Landis
 *
 */
public interface ProgramChairListener extends IMainFrameListener {

	/**
	 * Sets the decision.
	 * 
	 * @param decision Decision to change to.
	 */
	public abstract void setDecision(final PaperStatus decision, final Paper thePaper);
	
	/**
	 * Sets the SubProgram Chair.
	 * 
	 * @param theUser User to set as SubProgram Chair.
	 */
	public abstract void setSubPC(final User theUser);
	
	/**
	 * Add a paper to the SubProgram Chair.
	 * 
	 * @param thePaper The paper to add
	 * @return Returns whether the paper was successfully added.
	 */
	public abstract int addPaperToSubPC(final Paper thePaper, final SubProgramChair theSubPC);
	
}

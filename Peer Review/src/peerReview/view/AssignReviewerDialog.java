package peerReview.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;

import peerReview.model.Paper;
import peerReview.model.User;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Collection;

/**
 * Assign User Dialog from the subprogram chair.
 * 
 * @author Mike Dean
 * @revision Dain Landis
 *
 */
public final class AssignReviewerDialog extends JDialog {

	/** Serial id */
	private static final long serialVersionUID = -7604011356327440257L;
	
	private final JPanel contentPanel = new JPanel();
	private JTable table;
	private Collection<User> myUsers;
	private User retUser;

	/**
	 * Create the dialog popup dialog for adding reviewers.
	 * 
	 * @author Mike Dean
	 * 
	 * @param myPaperListener access to paper listener to obtain reviews.
	 * @param curPaper access to the current paper to match against reviews.
	 * @param myUsers the list of users for possible reviewers.
	 */
	public AssignReviewerDialog(Collection<User> theUsers, SubProgramChairListener myPaperListener, Paper curPaper) {
		super((java.awt.Frame) null, true);
		setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
		myUsers = theUsers;
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(58, 41, 293, 176);
			contentPanel.add(scrollPane);

			table = new JTable();
			DefaultTableModel myModel = new DefaultTableModel(new Object[0][0],
					new String[] { "Reviwer Name" }) {
				private static final long serialVersionUID = 1703626494274299609L;

				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};

			int i1 = 0;
			for(User u : myUsers){
				if(!curPaper.getAuthor().getName().equals(u.getName()) 
						&& myPaperListener.getReviews(u).size() < 4){
					boolean alreadyAdded = false;
					for(Paper p : myPaperListener.getReviews(u)){
						if(p.getTitle().equals(curPaper.getTitle())) {
							alreadyAdded = true;
						}
					}
					if(!alreadyAdded){
						i1++;
					}					
				}
			}
			
			final Object[][] data = new Object[i1][1];
			int i2 = 0;
			for(User u : myUsers){
				if(!curPaper.getAuthor().getName().equals(u.getName()) 
						&& myPaperListener.getReviews(u).size() < 4){
					boolean alreadyAdded = false;
					for(Paper p : myPaperListener.getReviews(u)){
						if(p.getTitle().equals(curPaper.getTitle())) {
							alreadyAdded = true;
						}
					}
					if(!alreadyAdded){
						data[i2][0] = u.getName();
						i2++;
					}					
				}
			}	
			myModel.setDataVector(data, new String[] { "Reviewer name" });
			table.setModel(myModel);
			scrollPane.setViewportView(table);
		}

		JLabel lblNewLabel = new JLabel("Select a User to Assign as Reviwer");
		lblNewLabel.setFont(new Font("SansSerif", Font.PLAIN, 16));
		lblNewLabel.setBounds(75, 19, 257, 16);
		contentPanel.add(lblNewLabel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if ((table.getSelectedRow() != -1)) {
							if ((table.getValueAt(table.getSelectedRow(), 0) != null)) {

								for (User p : myUsers) {
									if (p.getName() == table.getValueAt(
											table.getSelectedRow(), 0))
										if (table.getValueAt(
												table.getSelectedRow(), 0) != null)
											retUser = p;
									dispose();
								}
							}
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						AssignReviewerDialog.this.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    setVisible(true);

	}
	 User showDialog() {
	    return retUser;
	}
}

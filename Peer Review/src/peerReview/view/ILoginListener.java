/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.view;

/**
 * Interface for Login.
 * 
 * @author Mike Dean
 */
public interface ILoginListener {

	/**
	 * Attempts to login the user.
	 * 
	 * @param theUserName The user name to login.
	 * @return Whether the login succeeded.
	 */
	public abstract boolean login(final String theUserName);
	
}

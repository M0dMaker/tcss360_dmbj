/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.view;

import java.io.File;
import java.util.List;

import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.User;

/**
 * A callback listener for the main frame events.
 * 
 * @author Jacob Trimble
 * @author Dain Landis
 * @author Brandon Fjellman
 */
public interface IMainFrameListener {

	/** called when the program closes. */
	public abstract void close();
	/** Logs out the current user and shows the login screen. */
	public abstract void logout();
	/** @return Gets the current conference. */
	public abstract Conference getConference();
	/** @return Gets the current user. */
	public abstract User getCurrentUser();
	/**
	 * Downloads the given file.
	 * 
	 * @param theFile The file to open.
	 */
	public abstract void downloadFile(final File theFile);
	
	/**
	 * Gets a list of reviews.
	 * 
	 * @param theUser User to get reviews for.
	 * @return The list of reviews.
	 */
	public abstract List<Paper> getReviews(final User theUser);
	
	/**
	 * Changes the view.
	 */
	public abstract void setViewProgramChair();
	/**
	 * Changes the view.
	 */
	public abstract void setViewAuthor();
	/**
	 * Changes the view.
	 */
	public abstract void setViewSubProgramChair();
	/**
	 * Changes the view.
	 */
	public abstract void setViewReview();
}
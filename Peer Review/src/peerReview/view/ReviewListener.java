/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.view;

import java.io.File;
import java.util.List;

import peerReview.model.Paper;

/**
 * Interface for Review View, implemented in MainController.
 * 
 * @author Brandon Fjellman
 */
public interface ReviewListener extends IMainFrameListener{
	
	/**
	 * Submits a review to the Conference.
	 * 
	 * @param paper The paper that review is attached to.
	 * @param fileName The Review File.
	 * @param theRating The rating for the paper.
	 */
	public abstract void submitReview(final Paper paper, final File fileName, final int theRating);
	/**
	 * Gets a list of the current papers that the author has submitted.
	 * 
	 * @return The list of papers.
	 */
	public abstract List<Paper> getUserPapers();
	
	/**
	 * Returns a boolean to tell whether or not a review can still be submitted depending on Review submission date
	 * 
	 * @return True if it can be submitted, else false.
	 */
	public abstract boolean canSubmitReview();
}

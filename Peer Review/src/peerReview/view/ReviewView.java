/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collection;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import peerReview.model.Paper;
import peerReview.model.Review;


/**
 * Creates/Shows the ReviewView Screen.
 * 
 * @author Michael Dean 
 * @revisions Dain Landis
 */
public final class ReviewView implements IView {

	private final ReviewListener myPaperListener;
	private DefaultTableModel myModel;
	private JTable table;

	/**
	 * Assign Variables
	 * 
	 * @author Michael Dean
	 * 
	 * @param submitListener Passing in the main controller.
	 */
	public ReviewView(final ReviewListener submitListener) {
		myPaperListener = submitListener;
	}

	/**
	 * Display GUI elements.
	 * 
	 * @author Mike Dean
	 * @revision Dain Landis
	 */
	@Override
	public void show(final MainFrame theFrame) {
		final JPanel contentPane = theFrame.getPanel();
		theFrame.setHeader("Reviewer");
		theFrame.setTitle(myPaperListener.getConference().getName());
				
		final JScrollPane TablePane = new JScrollPane();
		TablePane.setViewportBorder(new EmptyBorder(0, 0, 0, 0));
		TablePane.setBounds(51, 152, 688, 349);
		contentPane.add(TablePane);

		table = new JTable();
		table.setBorder(new CompoundBorder());
		table.setShowVerticalLines(false);

		myModel = new DefaultTableModel(new Object[0][0], new String[] {"Rating", "Author", "Title" }) {

			private static final long serialVersionUID = 1703626494274299609L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.setModel(myModel);
		updatePapers();

		
		TablePane.setViewportView(table);

		final JButton btnDownloadPaper = new JButton("Download Paper");
		btnDownloadPaper.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/Download.png")));
		btnDownloadPaper.setSelectedIcon(null);
		btnDownloadPaper.setBounds(511, 515, 162, 41);
		btnDownloadPaper.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				final Collection<Paper> papers = myPaperListener.getConference().getSubmittedPapers();		
				if (table.getSelectedRow() != -1) {
					for(final Paper p : papers) {
						if (p.getTitle().equals(table.getValueAt(table.getSelectedRow(), 2))) {
							myPaperListener.downloadFile(p.getFileName());
						}
					}
				}
			}
		});
		contentPane.add(btnDownloadPaper);

		final JLabel lblSelectAConference = new JLabel("Reviewer Submission");	
		lblSelectAConference.setFont(new Font("Segoe UI", Font.PLAIN, 21));
		lblSelectAConference.setBounds(298, 116, 215, 28);
		contentPane.add(lblSelectAConference);

		final JButton btnCreateNewPaper = new JButton("Submit Review");
		btnCreateNewPaper.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/new.png")));
		btnCreateNewPaper.setBounds(146, 515, 157, 41);
		btnCreateNewPaper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int s = meowSlider(contentPane, "Enter Rating", "Submit Review");
				if (s != -1) {
					final File file = fileOpen();
					if (file != null) {
							final Collection<Paper> papers = myPaperListener.getConference().getSubmittedPapers();
							for(Paper p : papers) {							
							if (table.getSelectedRow() != -1) {
								if (p.getTitle().equals(
										table.getValueAt(
												table.getSelectedRow(), 2))) {
									
									myPaperListener.submitReview(p, file, s);
									updatePapers();
									btnCreateNewPaper.setText("Edit Review");
									btnCreateNewPaper.setIcon(new ImageIcon(this.getClass().getResource(
									"Icons/Update.png")));
								}
							}
						}
					}
				}
			}
		});

		
		contentPane.add(btnCreateNewPaper);
		
		//Actionlister if table is clicked on
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {	
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if(table.getSelectedRow()!= -1){
					if(table.getValueAt(table.getSelectedRow(), 0) != null) {
						btnCreateNewPaper.setText("Edit Review");
						btnCreateNewPaper.setIcon(new ImageIcon(this.getClass().getResource(
						"Icons/Update.png")));
					} else {
						btnCreateNewPaper.setText("Submit Review");
						btnCreateNewPaper.setIcon(new ImageIcon(this.getClass().getResource(
						"Icons/new.png")));
					}					
					btnCreateNewPaper.setEnabled(true);		
					btnDownloadPaper.setEnabled(true);
				}
			}
		});
		
		btnCreateNewPaper.setEnabled(false);
		btnDownloadPaper.setEnabled(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);		
	}

	/**
	 * Opens a JFilechooser to select a file.
	 * 
	 * @author Michael Dean
	 * 
	 * @return selected file
	 */
	private File fileOpen() {
		File selectedFile = null;
		JFileChooser fileChooser = new JFileChooser();
		int returnValue = fileChooser.showOpenDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			selectedFile = fileChooser.getSelectedFile();
		}
		return selectedFile;
	}
	
	/**
	 * Creates the panel for the JSlider
	 * 
	 * Name inspired by Michael Dean
	 * 
	 * @author Dain Landis
	 * @author Mike Dean
	 * 
	 * @param contentPane the content pane for the slider
	 * @param myMessage the message to be displayed
	 * @param myTitle the title panel
	 * @return The rating from the slider.
	 */
	private int meowSlider(JPanel contentPane, String myMessage, String myTitle) {
	    JOptionPane optionPane = new JOptionPane();
	    JSlider slider = getSlider(optionPane);
	    optionPane.setMessage(new Object[] {myMessage, slider });
	    optionPane.setMessageType(JOptionPane.QUESTION_MESSAGE);
	    optionPane.setOptionType(JOptionPane.OK_CANCEL_OPTION);
	    JDialog dialog = optionPane.createDialog(contentPane, myTitle);
	    dialog.setVisible(true);
	    if(Integer.parseInt(optionPane.getValue().toString()) == 2){
	    	// return a - 1 if user hit cancel. 
	    	return -1;
	    } else if (optionPane.getInputValue().toString().equals("uninitializedValue")) {
	    	// return a 3 for default value
	    	return 3;
	    }
	    // return value of slider
	    return Integer.parseInt(optionPane.getInputValue().toString());
	}
	
	/**
	 * Creates a JSlider to select between rating 1-5
	 * 
	 * @author Dain Landis
	 * @author Mike Dean
	 * 
	 * @param optionPane the pane the slider will be displayed in
	 * @return JSlider created slider
	 */
	static JSlider getSlider(final JOptionPane optionPane) {
		JSlider mySlider = new JSlider(1,5);
		mySlider.setMajorTickSpacing(1);
		mySlider.setPaintTicks(true);
		mySlider.setPaintLabels(true);
		ChangeListener changeListener = new ChangeListener() {
		public void stateChanged(ChangeEvent changeEvent) {
			JSlider theSlider = (JSlider) changeEvent.getSource();
		    if (!theSlider.getValueIsAdjusting()) {
		    	optionPane.setInputValue(new Integer(theSlider.getValue()));
		    }
		}};
		mySlider.addChangeListener(changeListener);
		return mySlider;
	}

	/**
	 * Updates papers in table
	 * 
	 * @author Mike Dean
	 */
	private void updatePapers() {
		final List<Paper> reviews = myPaperListener.getReviews(myPaperListener.getCurrentUser());
		final Object[][] data = new Object[reviews.size()][4];
		int i = 0;
		for (final Paper p : reviews) {
			data[i][1] = p.getAuthor().getName();
			
			for (final Review r : p.getReviews()) {
				if (myPaperListener.getCurrentUser().equals(r.getReviewer())) {
					data[i][0] = r.getRating();
				}
			}
			data[i][2] = p.getTitle();
			i++;
		}
		myModel.setDataVector(data, new String[] { "Rating", "Author", "Title" });
		
		table.getColumnModel().getColumn(0).setMaxWidth(50);
		table.getColumnModel().getColumn(1).setMaxWidth(200);
		table.getColumnModel().getColumn(1).setPreferredWidth(200);
	}

}

/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;

/**
 * Manages the main window and frame.
 * 
 * @author Mike Dean
 * @revision Jacob Trimble
 */
public final class MainFrame {

	/** Contains the main listener for callbacks. */
	private final IMainFrameListener myListener;
	/** Contains the main frame. */
	private final JFrame myFrame;
	/** Contains the scroll pane for the sub window. */
	private final JPanel myContentPane;
	/** Contains the combo box for the roles. */
	private JComboBox<String> myRoles;
	/** Whether to disable events. */
	private boolean myDisableEvents;

	/**
	 * Creates a new MainFrame object.
	 * 
	 * @author Mike Dean
	 * @revision Jacob Trimble
	 * 
	 * @param theConferences
	 */
	public MainFrame(final IMainFrameListener theListener) {
		try {
			for (final LookAndFeelInfo info : UIManager
					.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
		}
		myFrame = new JFrame();
		myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myFrame.setBounds(100, 100, 804, 615);
		myFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				myListener.close();
			}
		});

		myListener = theListener;

		myContentPane = new JPanel();
		myContentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		myContentPane.setLayout(null);

		myFrame.setContentPane(myContentPane);
		myDisableEvents = false;
	}

	/** 
	 * @author Jacob Trimble
	 * 
	 * @return The main content panel. 
	 */
	public JPanel getPanel() {
		return myContentPane;
	}

	/** 
	 * @author Jacob Trimble
	 * 
	 * Shows the main window. 
	 */
	public void show() {
		myFrame.setVisible(true);
	}

	/**
	 * Sets whether the current frame should have the header.
	 * 
	 * @author Mike Dean
	 * @revision Jacob Trimble
	 *
	 * @param theValue The name of the current view.
	 */
	public void setHeader(final String theValue) {
		myContentPane.removeAll();
		myContentPane.revalidate();
		myContentPane.repaint();

		if ("Login".equals(theValue)) {
			myFrame.setSize(378, 289);
		} else {
			myFrame.setSize(804, 615);

			final JButton btnLogout = new JButton("LogOut");
			btnLogout.setIcon(new ImageIcon(this.getClass().getResource(
					"Icons/logout.png")));
			btnLogout.setBounds(671, 22, 88, 28);
			btnLogout.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(final ActionEvent e) {
					myListener.logout();
				}
			});
			myContentPane.add(btnLogout);

			final JLabel lblUserName = new JLabel(myListener.getCurrentUser()
					.getName());
			lblUserName.setHorizontalAlignment(SwingConstants.RIGHT);
			lblUserName.setFont(new Font("Segoe UI", Font.PLAIN, 18));
			lblUserName.setBounds(457, 25, 208, 22);
			myContentPane.add(lblUserName);

			final JLabel lblConferenceTitle = new JLabel("");
			lblConferenceTitle.setIcon(new ImageIcon(this.getClass()
					.getResource("Icons/plain.png")));
			lblConferenceTitle.setFont(new Font("Segoe UI Emoji", Font.PLAIN,
					30));
			lblConferenceTitle.setBounds(105, 9, 228, 55);
			myContentPane.add(lblConferenceTitle);

			final JSeparator separator = new JSeparator();
			separator.setBounds(6, 66, 782, 12);
			myContentPane.add(separator);

			final JSeparator separator_1 = new JSeparator();
			separator_1.setBounds(6, 109, 635, 17);
			myContentPane.add(separator_1);
			
			final JLabel lblNewLabel = new JLabel("Today's Date:");
			lblNewLabel.setForeground(Color.GRAY);
			lblNewLabel.setBounds(644, 99, 80, 16);
			myContentPane.add(lblNewLabel);
			
			final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			final Date date = new Date();
			final JLabel lblNewLabel_1 = new JLabel("");
			lblNewLabel_1.setForeground(Color.GRAY);
			lblNewLabel_1.setBounds(721, 99, 78, 16);
			lblNewLabel_1.setText(dateFormat.format(date));
			myContentPane.add(lblNewLabel_1);


			final JLabel lblLogo = new JLabel("");
			final Image img = new ImageIcon(this.getClass().getResource(
					"Icons/logo4.png")).getImage();
			lblLogo.setIcon(new ImageIcon(img));
			lblLogo.setBounds(6, -6, 118, 76);
			myContentPane.add(lblLogo);
			
			if (myListener.getConference() != null) {				
				final JLabel lblLoggedInAs = new JLabel("Logged in as :");
				lblLoggedInAs.setFont(new Font("Segoe UI Emoji", Font.PLAIN, 16));
				lblLoggedInAs.setBounds(6, 81, 102, 16);
				myContentPane.add(lblLoggedInAs);
				
				myRoles = new JComboBox<String>();
				updateRoles();
				myRoles.setBounds(108, 75, 147, 26);
				myRoles.setSelectedItem(theValue);
				myRoles.addActionListener(new ActionListener() {				
					public void actionPerformed(final ActionEvent arg0) {
						if (!myDisableEvents) {
							if ("Program Chair".equals(myRoles.getSelectedItem())) {
								myListener.setViewProgramChair();
							} else if ("Author".equals(myRoles.getSelectedItem())) {
								myListener.setViewAuthor();
							}
							else if ("SubProgram Chair".equals(myRoles.getSelectedItem())) {
								myListener.setViewSubProgramChair();
							}
							else if ("Reviewer".equals(myRoles.getSelectedItem())) {
								myListener.setViewReview();
							}
						}
					}
				});
				myContentPane.add(myRoles);	
				
				addDates(myContentPane);
			}
		}
	}
	/**
	 * @author Mike Dean
	 * 
	 * @param title The title.
	 */
	public void setTitle(String title){
		myFrame.setTitle(title);
	}
	/**
	 * Updates the roles.
	 * 
	 * @author Jacob Trimble
	 * 
	 */
	public void updateRoles() {
		myDisableEvents = true;
		
		final Object name = myRoles.getSelectedItem();
		myRoles.removeAllItems();
		for(final String s : myListener.getConference().getRoles(myListener.getCurrentUser())){
			myRoles.addItem(s);
		}
		myRoles.setSelectedItem(name);
		
		myDisableEvents = false;
	}
	/**
	 * Adds submission and review deadlines to the header.
	 * 
	 * @author Mike Dean
	 * 
	 * @param contentPane
	 */
	private void addDates(final JPanel contentPane) {
		final JLabel lblReviewDeadline = new JLabel("Review DeadLine:");
		lblReviewDeadline.setForeground(Color.GRAY);
		lblReviewDeadline.setBounds(500, 138, 108, 16);
		contentPane.add(lblReviewDeadline);
		
		final JLabel lblAuthorDeadline = new JLabel("Author DeadLine:");
		lblAuthorDeadline.setForeground(Color.GRAY);
		lblAuthorDeadline.setBounds(54, 138, 102, 16);
		contentPane.add(lblAuthorDeadline);
		
		final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");		
		final JLabel lblReviewDate = new JLabel("11/30/2014");
		lblReviewDate.setForeground(Color.GRAY);
		lblReviewDate.setBounds(152, 138, 135, 16);
		contentPane.add(lblReviewDate);
		lblReviewDate.setText(dateFormat.format(myListener.getConference().getReviewDate()));

		final JLabel lblAuthorDate = new JLabel("11/30/2014");
		lblAuthorDate.setForeground(Color.GRAY);
		lblAuthorDate.setBounds(605, 138, 135, 16);
		contentPane.add(lblAuthorDate);
		lblAuthorDate.setText(dateFormat.format(myListener.getConference().getSubmissionDate()));
	}
	

}

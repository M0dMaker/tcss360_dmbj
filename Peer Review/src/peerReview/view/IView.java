/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.view;

/**
 * Main view interface through the MainController.
 * 
 * @author Mike Dean
 */
public interface IView {

	/**
	 * Display the view.
	 * 
	 * @param theFrame The main frame.
	 */
	public abstract void show(final MainFrame theFrame);
	
}

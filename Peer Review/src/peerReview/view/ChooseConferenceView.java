/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.view;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Collection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import peerReview.model.Conference;

/**
 * Creates/Shows the Choose Conference View Screen.
 * 
 * @author Michael Dean
 * @revisions Jacob Trimble
 */
public final class ChooseConferenceView implements IView {

	public final IChooseConferenceListener myChooseListener;
	private JTable table;
	
	
	public ChooseConferenceView(final IChooseConferenceListener theChooseListener) {
		myChooseListener = theChooseListener;
	}
	
	/**
	 * Displays the GUI to the screen.
	 * 
	 * @author Mike Dean
	 * @revision Jacob Trimble
	 */
	@Override
	public void show(final MainFrame theFrame) {
		final JPanel contentPane = theFrame.getPanel();
		theFrame.setHeader("ChooseConference");
		
		final JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new EmptyBorder(0, 0, 0, 0));
		scrollPane.setBounds(51, 152, 688, 349);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setBorder(new CompoundBorder());
		table.setShowVerticalLines(false);
		table.setModel(new DefaultTableModel(
			getData(myChooseListener.getConferences()),
			new String[] {
				"Conference Name", "Program Chair", "Paper DeadLine"
			}
		) {
			private static final long serialVersionUID = -832648793172863433L;
			
			@Override
			public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		});
		table.getColumnModel().getColumn(0).setPreferredWidth(300);
		table.getColumnModel().getColumn(1).setPreferredWidth(100);
		table.getColumnModel().getColumn(2).setPreferredWidth(140);
		table.getColumnModel().getColumn(2).setMaxWidth(140);
		//table.getColumnModel().getColumn(2).setMaxWidth(125);
		table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane.setViewportView(table);
		
		final JButton btnOpenConference = new JButton("Open Conference");
		btnOpenConference.setIcon(new ImageIcon(this.getClass().getResource("Icons/okay2.png")));
		btnOpenConference.setSelectedIcon(null);
		btnOpenConference.setBounds(302, 515, 162, 41);
		btnOpenConference.setEnabled(false);
		btnOpenConference.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				if (table.getSelectedRow() != -1) {
					myChooseListener.chooseConference((Conference) table.getModel().getValueAt(table.getSelectedRow(), 0));
				}
			}
		});
		contentPane.add(btnOpenConference);
		
		final JLabel lblSelectAConference = new JLabel("Select a Conference");
		lblSelectAConference.setFont(new Font("Segoe UI", Font.PLAIN, 21));
		lblSelectAConference.setBounds(299, 116, 189, 28);
		contentPane.add(lblSelectAConference);
		
		final JLabel lblLogo = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("Icons/logo4.png")).getImage();
		lblLogo.setIcon(new ImageIcon(img));
		lblLogo.setBounds(6, -6, 118, 76);
		contentPane.add(lblLogo);
		
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
		    @Override
		    public void valueChanged(ListSelectionEvent event) {
		        if (table.getSelectedRow() > -1) {		    		
		    		btnOpenConference.setEnabled(true);
		        }
		    }
		});
	}

	/**
	 * Creates the data to be displayed in table.
	 * 
	 * @author Mike Dean
	 * @revision Jacob Trimble
	 * 
	 * @param theConferences List of conferences
	 * @return Data for table as Object[][]
	 */
	private Object[][] getData(final Collection<Conference> theConferences) {
		final Object[][] ret = new Object[theConferences.size()][4];
		final SimpleDateFormat ft = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss"); 
		int i = 0;
		for (final Conference c : theConferences) {
			ret[i][0] = c;
			ret[i][1] = c.getProgramChair().getName();
			ret[i][2] = ft.format(c.getSubmissionDate());
			i++;
		}
		
		return ret;
	}
	
}

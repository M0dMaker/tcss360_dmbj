package peerReview.view;

import java.io.File;
import java.util.List;

import peerReview.model.Paper;
import peerReview.model.SubProgramChair;
import peerReview.model.User;

/**
 * Listener for SubProgramChair view.
 * 
 * @author Brandon Fjellman
 *
 */
public interface SubProgramChairListener extends IMainFrameListener {
	
	/**
	 * Assigns a paper to a reviewer.
	 * 
	 * @param thePaper The paper to be assigned.
	 * @param theReviewer The reviewer to review.
	 * @return Integer value 0 no errors, 1 and 2 errors.
	 */
	public abstract int assignPaper(Paper thePaper, User theReviewer);
	
	/**
	 * Submits a recommendation to the conference.
	 * 
	 * @param theSubProgramChair The SPC making the recommendation.
	 * @param thePaper The paper receiving the recommendation.
	 * @param theFileName The file of the recommendation.
	 */
	public abstract void submitRecommentation(SubProgramChair theSubProgramChair, Paper thePaper, File theFileName);
	
	/**
	 * Gets a list of reviewers for a paper.
	 * 
	 * @param thePaper The paper to get reviewers from.
	 * 
	 * @return List of Users that are doing reviews.
	 */
	public abstract List<User> getReviewers(Paper thePaper);

}

/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Collection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import peerReview.model.Paper;
import peerReview.model.PaperStatus;
import peerReview.model.Review;

/**
 * Create/Show the author view
 * 
 * @author Michael Dean 
 * @revisions Dain Landis
 * @revision Jacob Trimble
 * 
 */
public final class AuthorView implements IView {

	private final AuthorListener myPaperListener;
	private DefaultTableModel myModel;
	private JTable table;
	private Object[][] data1 = null;
	protected Paper curPaper;

	public AuthorView(final AuthorListener submitListener) {
		myPaperListener = submitListener;
	}
	
	/**
	 * Displays the GUI to the screen.
	 * 
	 * @author Mike Dean
	 * @revision Dain Landis
	 * @revision Jacob Trimble
	 */
	@Override
	public void show(final MainFrame theFrame) {
		final JPanel contentPane = theFrame.getPanel();
		theFrame.setHeader("Author");
		theFrame.setTitle(myPaperListener.getConference().getName());

		final JScrollPane TablePane = new JScrollPane();
		TablePane.setViewportBorder(new EmptyBorder(0, 0, 0, 0));
		TablePane.setBounds(51, 152, 688, 349);
		contentPane.add(TablePane);

		table = new JTable();
		table.setBorder(new CompoundBorder());
		table.setShowVerticalLines(false);

		myModel = new DefaultTableModel(new Object[0][0], new String[] {
				"Status", "Deadline", "Title " }) {

			private static final long serialVersionUID = 1703626494274299609L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.setModel(myModel);
		updatePapers();

		table.getColumnModel().getColumn(0).setPreferredWidth(100);
		table.getColumnModel().getColumn(0).setMaxWidth(100);
		table.getColumnModel().getColumn(1).setMaxWidth(100);
		TablePane.setViewportView(table);

		final JButton btnDeletePaper = new JButton("Delete Paper");
		btnDeletePaper.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/delet.png")));
		btnDeletePaper.setSelectedIcon(null);
		btnDeletePaper.setBounds(415, 515, 135, 41);
		contentPane.add(btnDeletePaper);
		btnDeletePaper.setEnabled(false);
		btnDeletePaper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				for (Paper p : myPaperListener.getUserPapers()) {
					if (table.getSelectedRow() != -1) {
						if (p.getTitle().equals(
								table.getValueAt(table.getSelectedRow(), 2))) {
							myPaperListener.removePaper(p);
							updatePapers();
						}
					}
				}
			}
		});

		final JLabel lblSelectAConference = new JLabel("Manuscript Submission");
		lblSelectAConference.setFont(new Font("Segoe UI", Font.PLAIN, 21));
		lblSelectAConference.setBounds(298, 116, 215, 28);
		contentPane.add(lblSelectAConference);

		final JButton btnUpdatePaper = new JButton("Update Paper");
		btnUpdatePaper.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/Update.png")));
		btnUpdatePaper.setBounds(586, 515, 132, 41);
		contentPane.add(btnUpdatePaper);
		btnUpdatePaper.setEnabled(false);
		btnUpdatePaper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				for (Paper p : myPaperListener.getUserPapers()) {
					if (table.getSelectedRow() != -1) {
						if (p.getTitle().equals(
								table.getValueAt(table.getSelectedRow(), 2))) {
							if (myPaperListener.canSubmitPaper()) {
								String s = CreatePaperDialog(contentPane,
										"Enter New Paper Title",
										"Update Paper Title", p.toString());
								while (s != null && s.length() == 0) {
									s = CreatePaperDialog(contentPane,
											"Enter New Paper Title",
											"Update Paper Title", p.toString());
								}
								if (s != null) {
									final File myFile = fileOpen();
									if (myFile != null) {
										myPaperListener.editPaper(p, s, myFile);
										updatePapers();
									}
								}
							} else {
								JOptionPane
										.showMessageDialog(
												contentPane,
												"Cannot submit a paper past the deadline.",
												"Cannot Submit Paper",
												JOptionPane.PLAIN_MESSAGE);
							}
						}
					}
				}
			}
		});

		final JButton btnCreateNewPaper = new JButton("Create New Paper");
		btnCreateNewPaper.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (myPaperListener.canSubmitPaper()) {
					String s = CreatePaperDialog(contentPane,
							"Enter Paper Title", "Paper Title", "");
					// if(s != null && )
					while (s != null && s.length() == 0) {
						s = CreatePaperDialog(contentPane, "Enter Paper Title",
								"Paper Title", "");
					}
					if (s != null) {
						final File file = fileOpen();
						if (file != null) {
							myPaperListener.submitPaper(file, s);
							updatePapers();
						}
					}
				} else {
					JOptionPane.showMessageDialog(contentPane,
							"Cannot submit a paper past the deadline.",
							"Cannot Submit Paper", JOptionPane.PLAIN_MESSAGE);
				}
			}
		});

		btnCreateNewPaper.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/new.png")));
		btnCreateNewPaper.setBounds(228, 515, 157, 41);
		if (!myPaperListener.canSubmitPaper()) {
			btnCreateNewPaper.setEnabled(false);
			btnDeletePaper.setEnabled(false);
		}
		contentPane.add(btnCreateNewPaper);

		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent arg0) {
						if (table.getSelectedRow() != -1
								&& myPaperListener.canSubmitPaper()) {
							btnDeletePaper.setEnabled(true);
						}
					}
				});
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		//Popup Section
		final JPanel PopUpPanel = new JPanel();
		PopUpPanel.setBorder(new TitledBorder(null, "Paper Details",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		PopUpPanel.setBounds(150, 243, 438, 255);

		PopUpPanel.setLayout(null);

		JScrollPane reviewScrollPane = new JScrollPane();
		reviewScrollPane.setBounds(104, 65, 248, 135);
		PopUpPanel.add(reviewScrollPane);

		JLabel lblReviewOnPaper = new JLabel("Reviews on Selected Paper");
		lblReviewOnPaper.setBounds(123, 38, 198, 16);
		PopUpPanel.add(lblReviewOnPaper);
		lblReviewOnPaper.setFont(new Font("SansSerif", Font.PLAIN, 16));
		JButton btnDownload = new JButton("Download Reviews");
		btnDownload.setBounds(150, 209, 155, 28);
		PopUpPanel.add(btnDownload);
		contentPane.add(PopUpPanel);
		btnDownload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	
				for(Review r :curPaper.getReviews()){
					myPaperListener.downloadFile(r.getFileName());	
				}	
			}
		});
		btnDownload.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/download1.png")));

		JButton btnClose = new JButton("");
		btnClose.setBounds(399, 6, 30, 28);
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PopUpPanel.setVisible(false);
				TablePane.setVisible(true);
			}
		});
		btnClose.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/delet.png")));
		PopUpPanel.add(btnClose);

		// POPUP TABLE
		final JTable popUpTable = new JTable();
		reviewScrollPane.setViewportView(popUpTable);
		final DefaultTableModel myModel2 = new DefaultTableModel(
				new Object[0][0], new String[] {}) {
			private static final long serialVersionUID = 1703626494274299609L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		popUpTable.setModel(myModel2);

		final JButton btnDetails = new JButton("Show Reviews");
		btnDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PopUpPanel.setVisible(true);
				TablePane.setVisible(false);
			}
		});
		btnDetails.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/show.png")));
		btnDetails.setBounds(60, 515, 138, 41);
		btnDetails.setEnabled(false);
		contentPane.add(btnDetails);
		btnUpdatePaper.setEnabled(false);

		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						if (table.getSelectedRow() != -1) {
							if (table.getValueAt(table.getSelectedRow(), 0)
									.equals(PaperStatus.Rejected)) {
								btnDetails.setEnabled(true);
								btnUpdatePaper.setEnabled(false);
							} else if (table.getValueAt(table.getSelectedRow(),
									0).equals(PaperStatus.Accepted)) {
								btnDetails.setEnabled(true);
								btnUpdatePaper.setEnabled(false);
							} else {
								btnDetails.setEnabled(false);
								btnUpdatePaper.setEnabled(true);
							}
						}
						
						
						if (table.getSelectedRow() != -1) {
							for (Paper p : myPaperListener.getUserPapers()) {
								if (table.getValueAt(table.getSelectedRow(), 2) == p
										.getTitle()) {
									curPaper = p;
									int i1 = 0;

									data1 = new Object[p.getReviews().size()][2];
									for (Review x : p.getReviews()) {
										data1[i1][0] = x.getRating();
										data1[i1][1] = p.getStatus();
										i1++;
									}
								}
							}

						}
						popUpTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
						popUpTable.setEnabled(false);
						myModel2.setDataVector(data1, new String[] { 
								"Rating", "Status" });
					}
				});
		
		PopUpPanel.setVisible(false);
	}

	/**
	 * JFile chooser for creating a paper
	 * 
	 * @author Mike Dean
	 * 
	 * @return file of paper
	 */
	private File fileOpen() {
		File selectedFile = null;
		JFileChooser fileChooser = new JFileChooser();
		int returnValue = fileChooser.showOpenDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			selectedFile = fileChooser.getSelectedFile();
		}
		return selectedFile;
	}

	/**
	 * Creates pop up window for Creating papers
	 * 
	 * @author Mike Dean
	 * 
	 * @param contentPane content pane of main window
	 * @param myMessage message for window
	 * @param myTitle title of paper
	 * @param theTextMessage message for window
	 * @return title of paper
	 */
	private String CreatePaperDialog(JPanel contentPane, String myMessage,
			String myTitle, String theTextMessage) {
		String s = (String) JOptionPane.showInputDialog(contentPane, myMessage,
				myTitle, JOptionPane.PLAIN_MESSAGE, null, null, theTextMessage);
		return s;
	}

	/**
	 * Update papers in list
	 * 
	 * @author Mike Dean
	 * 
	 */
	private void updatePapers() {
		final Collection<Paper> papers = myPaperListener.getUserPapers();
		final Object[][] data = new Object[papers.size()][3];
		final SimpleDateFormat ft = new SimpleDateFormat("MM-dd-yyyy");

		int i = 0;
		for (final Paper p : papers) {
			data[i][0] = p.getStatus();
			data[i][1] = ft.format(myPaperListener.getConference()
					.getSubmissionDate());
			data[i][2] = p.getTitle();
			i++;
		}

		myModel.setDataVector(data, new String[] { "Status", "Deadline",
				"Title " });
		table.getColumnModel().getColumn(0).setPreferredWidth(100);
		table.getColumnModel().getColumn(0).setMaxWidth(100);
		table.getColumnModel().getColumn(1).setMaxWidth(100);
	}
}

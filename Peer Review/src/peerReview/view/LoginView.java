/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Creates and displays the LoginView
 * 
 * @author Mike Dean
 */
public final class LoginView implements IView {
	
	/** Contains the listener. */
	private final ILoginListener myLoginListener;
	
	public LoginView(final ILoginListener theLoginListener) {
		myLoginListener = theLoginListener;
	}
	
	/**
	 * Displays the GUI.
	 * 
	 * @author Mike Dean
	 */
	@Override
	public void show(final MainFrame theFrame) {
		final JPanel contentPane = theFrame.getPanel();
		theFrame.setHeader("Login");
		theFrame.setTitle("");
		
		final JTextField textField = new JTextField();
		textField.setBounds(117, 152, 122, 28);
		contentPane.add(textField);
		textField.setColumns(10);
		
		final JLabel lblInvalid = new JLabel("");
		lblInvalid.setForeground(Color.RED);
		lblInvalid.setBounds(123, 131, 122, 16);
		contentPane.add(lblInvalid);
		
		final ActionListener onLogin = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				myLoginListener.login(textField.getText());
				if(myLoginListener.login(textField.getText()) == false)
					lblInvalid.setText("Invalid User Name");
			}
		};
		
		textField.addActionListener(onLogin);
		
		final JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(onLogin);
		final Image login = new ImageIcon(this.getClass().getResource("Icons/okay2.png")).getImage();
		btnLogin.setIcon(new ImageIcon(login));
		btnLogin.setSelectedIcon(null);
		btnLogin.setBounds(133, 188, 88, 28);
		contentPane.add(btnLogin);
		
		final JLabel lblNewLabel_1 = new JLabel("");
		final Image logo = new ImageIcon(this.getClass().getResource("Icons/logo4.png")).getImage();
		lblNewLabel_1.setIcon(new ImageIcon(logo));
		lblNewLabel_1.setBounds(22, -22, 109, 134);
		contentPane.add(lblNewLabel_1);
		
		final JLabel lblNewLabel_2 = new JLabel("");
		final Image plain = new ImageIcon(this.getClass().getResource("Icons/plain.png")).getImage();
		lblNewLabel_2.setIcon(new ImageIcon(plain));
		lblNewLabel_2.setBounds(117, 20, 239, 64);
		contentPane.add(lblNewLabel_2);
		
		final JLabel lblEnterUserName = new JLabel("Enter User name");
		lblEnterUserName.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		lblEnterUserName.setBounds(106, 96, 166, 37);
		contentPane.add(lblEnterUserName);
	}
	
}

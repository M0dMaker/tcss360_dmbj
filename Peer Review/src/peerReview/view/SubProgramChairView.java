/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.PaperStatus;
import peerReview.model.Review;
import peerReview.model.SubProgramChair;
import peerReview.model.User;

/**
 * View for the subprogram chair role.
 * 
 * @author Mike Dean
 * @revision Jacob Trimble
 * @revision Dain Landis
 * @revision Brandon Fjellman
 */
public final class SubProgramChairView implements IView {

	private final SubProgramChairListener myPaperListener;
	private DefaultTableModel myModel;
	private Conference myConference;
	private Collection<User> myUsers;
	private Paper curPaper;
	private JButton btnAssignReviewer;
	private JButton btnDownload;
	private JButton btnUploadRecommendation;

	public SubProgramChairView(final SubProgramChairListener submitListener,
			Conference theConference, Collection<User> theUsers) {
		myPaperListener = submitListener;
		myConference = theConference;
		myUsers = theUsers;
	}

	/**
	 * Displays the GUI.
	 * 
	 * @author Mike Dean
	 * @revision Jacob Trimble
	 * @revision Dain landis
	 * @revision Brandon Fjellman
	 */
	@Override
	public void show(final MainFrame theFrame) {

		final JPanel contentPane = theFrame.getPanel();
		theFrame.setHeader("SubProgram Chair");
		theFrame.setTitle(myPaperListener.getConference().getName());

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new EmptyBorder(0, 0, 0, 0));
		scrollPane.setBounds(51, 152, 688, 93);

		contentPane.add(scrollPane);

		final JTable tableTop = new JTable();
		tableTop.setBorder(new CompoundBorder());
		tableTop.setShowVerticalLines(false);

		// Populate the top table
		updateTopTable(tableTop);

		tableTop.getColumnModel().getColumn(0).setPreferredWidth(300);
		tableTop.getColumnModel().getColumn(1).setPreferredWidth(100);

		scrollPane.setViewportView(tableTop);

		JLabel lblSelectAConference = new JLabel("SubProgram Chair Manager");
		lblSelectAConference.setFont(new Font("Segoe UI", Font.PLAIN, 21));
		lblSelectAConference.setBounds(270, 116, 400, 28);

		contentPane.add(lblSelectAConference);

		final JLabel lblTheWayBack = new JLabel("");
		lblTheWayBack.setBounds(221, 310, 500, 19);
		contentPane.add(lblTheWayBack);
		lblTheWayBack.setFont(new Font("SansSerif", Font.BOLD, 14));
		lblTheWayBack.setForeground(SystemColor.windowText);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(157, 340, 456, 120);
		contentPane.add(scrollPane_1);

		final JTable tableBottom = new JTable();

		// Populate the bottom Table
		scrollPane_1.setViewportView(tableBottom);

		// Action listener when top table is clicked on
		tableTop.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent event) {
						if (tableTop.getSelectedRow() > -1) {
							if (tableTop.getValueAt(tableTop.getSelectedRow(),
									0) != null) {
								updateBottomTable(
										tableBottom,
										(String) tableTop.getValueAt(
												tableTop.getSelectedRow(), 0),
										lblTheWayBack);
								btnAssignReviewer.setEnabled(true);
								btnUploadRecommendation.setEnabled(true);

								//
								for (SubProgramChair sub : myConference
										.getSubProgramChairs()) {
									if (myPaperListener.getCurrentUser()
											.getName()
											.equals(sub.getUser().getName())) {
										for (Paper paper : sub.getPapers()) {
											if (tableTop.getValueAt(
													tableTop.getSelectedRow(),
													0).equals(paper.getTitle())) {
												if (paper.getStatus()
														.toString()
														.equals("Recommended")) {
													btnUploadRecommendation
															.setEnabled(false);
												} else
													btnUploadRecommendation
															.setEnabled(true);

											}
										}
									}
								}
								if (tableBottom.getSelectedRow() < 0) {
									btnDownload.setEnabled(false);
								}
							}
						}
					}
				});

		// Action listener when Bottom table is clicked on
		tableBottom.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent event) {
						if (tableBottom.getSelectedRow() > -1) {
							if (tableBottom.getValueAt(
									tableBottom.getSelectedRow(), 0) != null) {
								btnDownload.setEnabled(true);
								btnUploadRecommendation.setEnabled(true);
							}
							if (tableBottom.getValueAt(
									tableBottom.getSelectedRow(), 0) == null) {
								btnDownload.setEnabled(false);
							}
						}
					}
				});

		btnAssignReviewer = new JButton("Assign Reviewer");
		btnAssignReviewer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AssignReviewerDialog dialog = new AssignReviewerDialog(myUsers,
						myPaperListener, curPaper);
				dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				if (dialog.showDialog() != null)
					myPaperListener.assignPaper(curPaper, dialog.showDialog());
				theFrame.updateRoles();
				updateBottomTable(tableBottom, (String) tableTop.getValueAt(
						tableTop.getSelectedRow(), 0), lblTheWayBack);
			}
		});

		btnAssignReviewer.setBounds(305, 485, 150, 41);
		btnAssignReviewer.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/assign.png")));
		contentPane.add(btnAssignReviewer);

		btnDownload = new JButton("DownLoad ");
		btnDownload.setBounds(124, 485, 132, 41);
		btnDownload.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				final Collection<Paper> papers = myConference
						.getSubmittedPapers();
				for (final Paper p : papers) {
					if (p.getTitle().equals(
							tableTop.getValueAt(tableTop.getSelectedRow(), 0))) {
						for (final Review r : p.getReviews()) {
							if (tableBottom.getValueAt(
									tableBottom.getSelectedRow(), 1).equals(
									r.getReviewer().getName())) {
								myPaperListener.downloadFile(r.getFileName());
							}
						}
					}
				}
			}
		});
		btnDownload.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/download2.png")));
		contentPane.add(btnDownload);

		btnUploadRecommendation = new JButton("Recommend");
		btnUploadRecommendation.setBounds(500, 485, 147, 41);
		btnUploadRecommendation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (SubProgramChair sub : myConference.getSubProgramChairs()) {
					if (myPaperListener.getCurrentUser().getName()
							.equals(sub.getUser().getName())) {
						for (Paper paper : sub.getPapers()) {
							if (tableTop.getSelectedRow() != -1) {
								if (paper.getTitle().equals(
										tableTop.getValueAt(
												tableTop.getSelectedRow(), 0))) {
									final File file = fileOpen();
									if (file != null) {
										myPaperListener.submitRecommentation(
												sub, paper, file);
										btnUploadRecommendation
												.setEnabled(false);
										updateTopTable(tableTop);
									}
								}
							}
						}
					}
				}

			}
		});
		btnUploadRecommendation.setIcon(new ImageIcon(this.getClass()
				.getResource("Icons/Update.png")));
		contentPane.add(btnUploadRecommendation);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Current Paper Selected",
				TitledBorder.LEFT, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBackground(UIManager.getColor("ArrowButton.background"));
		panel.setBounds(51, 281, 688, 268);

		contentPane.add(panel);

		btnAssignReviewer.setEnabled(false);
		btnDownload.setEnabled(false);
		btnUploadRecommendation.setEnabled(false);

	}

	/**
	 * Update the bottom table.
	 * 
	 * @author Michael Dean
	 * 
	 * @param table
	 *            table the needs updating
	 * @param title
	 *            title of the paper
	 * @param lblTheWayBack
	 *            label to display title name
	 */
	private void updateBottomTable(JTable table, String title,
			JLabel lblTheWayBack) {
		myModel = new DefaultTableModel(new Object[0][0], new String[] {
				"Rating", "Reviewer" }) {
			private static final long serialVersionUID = 1703626494274299609L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		final Collection<Paper> papers = myConference.getSubmittedPapers();
		for (Paper p : papers) {
			if (p.getTitle().equals(title)) {
				table.setModel(myModel);
				curPaper = p;
				final Object[][] data = new Object[myPaperListener
						.getReviewers(p).size()][3];
				int i1 = 0;
				lblTheWayBack.setText(p.getTitle());

				for (User reviwers : myPaperListener.getReviewers(p)) {

					for (final Review r : p.getReviews()) {
						if (reviwers.getName()
								.equals(r.getReviewer().getName())) {
							data[i1][0] = r.getRating();
						}
					}
					data[i1][1] = reviwers.getName();
					i1++;
				}
				myModel.setDataVector(data,
						new String[] { "Rating", "Reviewer" });
				table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			}
		}

	}

	/**
	 * Update top table
	 * 
	 * @author Mike Dean
	 * 
	 * @param table
	 *            The top table needing an update.
	 */
	private void updateTopTable(JTable table) {
		myModel = new DefaultTableModel(new Object[0][0], new String[] {
				"Title", "Author", "Recomendation" }) {
			private static final long serialVersionUID = 1703626494274299609L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.setModel(myModel);

		int tableSize = 0;
		for (SubProgramChair sub : myConference.getSubProgramChairs()) {
			if (myPaperListener.getCurrentUser().getName()
					.equals(sub.getUser().getName())) {
				for (@SuppressWarnings("unused")
				Paper x : sub.getPapers()) {
					tableSize++;
				} // Determine how big the table should be
			}
		}

		final Object[][] data = new Object[tableSize][3];
		for (SubProgramChair sub : myConference.getSubProgramChairs()) {
			if (myPaperListener.getCurrentUser().getName()
					.equals(sub.getUser().getName())) {
				int i1 = 0;
				for (Paper x : sub.getPapers()) {
					data[i1][0] = x.getTitle();
					data[i1][1] = x.getAuthor().getName();
					data[i1][2] = (x.getStatus() == PaperStatus.Recommended
							|| x.getStatus() == PaperStatus.Accepted || x
							.getStatus() == PaperStatus.Rejected);
					i1++;
				}
			}
		}
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		myModel.setDataVector(data, new String[] { "Title", "Author",
				"Recomendation" });
	}

	/**
	 * Opens a JFile chooser to select file
	 * 
	 * @author Mike Dean
	 * 
	 * @return file chosen
	 */
	private File fileOpen() {
		File selectedFile = null;
		JFileChooser fileChooser = new JFileChooser();
		int returnValue = fileChooser.showOpenDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			selectedFile = fileChooser.getSelectedFile();
		}
		return selectedFile;
	}

}

package peerReview.view;

import java.io.File;
import java.util.List;

import peerReview.model.Paper;

/**
 * 
 * @author Brandon Fjellman
 *
 */
public interface AuthorListener extends IMainFrameListener {
	
	/**
	 * Submits a Paper to the conference.
	 * 
	 * @param fileName The file name of the paper.
	 * @param title The title of the paper.
	 * @return If submission was successful.
	 */
	public abstract boolean submitPaper(final File fileName, final String title);
	
	/**
	 * @return Whether an author can submit a paper in the current conference.
	 */
	public abstract boolean canSubmitPaper();
	
	/**
	 * Gets a list of the current papers that the author has submitted.
	 * 
	 * @return The list of papers.
	 */
	public abstract List<Paper> getUserPapers();
	
	/**
	 * Edits the paper with new title and file.
	 * 
	 * @param thePaper The paper to have the title edited.
	 */
	public abstract void editPaper(Paper thePaper, String title, File theFile);
	
	/**
	 * Removes the paper from the Conference.
	 * 
	 * @param thePaper The paper to remove.
	 */
	public abstract void removePaper(Paper thePaper);
	

}

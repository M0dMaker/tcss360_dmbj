/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;

import peerReview.model.Paper;
import peerReview.model.PaperStatus;
import peerReview.model.Recommendation;
import peerReview.model.Review;
import peerReview.model.SubProgramChair;

/**
 * Creates/Shows the Program chair view screen.
 * 
 * @author Michael Dean
 * @revision Jacob Trimble
 * @revision Dain Landis
 * @revision Brandon Fjellman
 */
public final class ProgramChairView implements IView {

	/** Contains the listener for callback methods. */
	private final ProgramChairListener myPaperListener;
	/** Contains the combo boxes for the sub-program chairs. */
	private final List<JComboBox<SubProgramChair>> myBoxSPC;
	/** Contains the combo boxes for the paper status. */
	private final List<JComboBox<PaperStatus>> myBoxStatus;
	/** Whether to disable events currently. */
	private boolean myDisableEvents;
	private JTable table;
	private JScrollPane scrollPane;
	private Paper curPaper;

	/**
	 * Creates a new ProgramChairView with the given listener.
	 * 
	 * @author Mike Dean
	 * 
	 * @param submitListener The listener for callback methods.
	 */
	public ProgramChairView(final ProgramChairListener submitListener) {
		myPaperListener = submitListener;
		myBoxSPC = new ArrayList<JComboBox<SubProgramChair>>();
		myBoxStatus = new ArrayList<JComboBox<PaperStatus>>();
		myDisableEvents = false;
	}

	/** 
	 * Adds any needed items to the frame and shows the view.
	 * 
	 *  @author Mike Dean
	 *  @revision Dain Landis
	 *  @revision Jacob Trimble
	 *  @revision Brandon Fjellman
	 * 
	 */
	@Override
	public void show(final MainFrame theFrame) {
		final JPanel contentPane = theFrame.getPanel();
		theFrame.setHeader("Program Chair");
		theFrame.setTitle(myPaperListener.getConference().getName());

		final JLabel lblSelectAConference = new JLabel("ProgramChair View");
		lblSelectAConference.setFont(new Font("Segoe UI", Font.PLAIN, 21));
		lblSelectAConference.setBounds(298, 116, 215, 28);
		contentPane.add(lblSelectAConference);

		scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new EmptyBorder(0, 0, 0, 0));
		scrollPane.setBounds(51, 152, 688, 349);
		contentPane.add(scrollPane);

		final List<Paper> papers = myPaperListener.getConference()
				.getSubmittedPapers();
		final List<DefaultCellEditor> editors = new ArrayList<DefaultCellEditor>();
		final List<DefaultCellEditor> editorsStatus = new ArrayList<DefaultCellEditor>();
		final Object[][] data = new Object[papers.size()][3];
		final String[] columnNames = new String[] { "Set Status", "Title",
				"Assign SubProgramChair" };

		for (int i = 0; i < papers.size(); i++) {
			final Paper p = papers.get(i);
			final JComboBox<SubProgramChair> cbSpc = new JComboBox<SubProgramChair>();
			final JComboBox<PaperStatus> cpStatus = new JComboBox<PaperStatus>();

			editors.add(new DefaultCellEditor(cbSpc));
			editorsStatus.add(new DefaultCellEditor(cpStatus));
			myBoxSPC.add(cbSpc);
			myBoxStatus.add(cpStatus);

			cbSpc.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(final ActionEvent event) {
					if (!myDisableEvents) {
						if (cbSpc.getSelectedItem() != null
								&& cbSpc.getSelectedItem() != SubProgramChair.NOT_SELECTED) {
							myPaperListener.addPaperToSubPC(p,
									(SubProgramChair) cbSpc.getSelectedItem());
						}
						updateBoxes();
					}
				}
			});
			cpStatus.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(final ActionEvent event) {
					if (!myDisableEvents) {
						if (cpStatus.getSelectedItem() == PaperStatus.Accepted
								|| cpStatus.getSelectedItem() == PaperStatus.Rejected) {
							myPaperListener.setDecision(
									(PaperStatus) cpStatus.getSelectedItem(), p);
						}
						updateBoxes();
					}
				}
			});

			data[i][0] = p.getStatus();
			data[i][1] = p;
			if (p.getSubProgramChair() == null) {
				data[i][2] = SubProgramChair.NOT_SELECTED;
			} else {
				data[i][2] = p.getSubProgramChair();
			}
		}

		updateBoxes();

		final DefaultTableModel model = new DefaultTableModel(data, columnNames);
		table = new JTable(model) {
			private static final long serialVersionUID = 399688261958803757L;

			@Override
			public TableCellEditor getCellEditor(final int row, final int column) {
				final int modelColumn = convertColumnIndexToModel(column);

				if (modelColumn == 2 && row < editors.size())
					return editors.get(row);
				else if (modelColumn == 0 && row < editorsStatus.size())
					return editorsStatus.get(row);
				else
					return super.getCellEditor(row, column);
			}

			public boolean isCellEditable(int row, int column) {
				if (column == 1)
					return false;
				return true;
			}
		};

		//Popup Panel
		final JPanel PopUpPanel = new JPanel();
		PopUpPanel.setBorder(new TitledBorder(null, "Paper Details",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		PopUpPanel.setBounds(119, 243, 547, 255);
		PopUpPanel.setLayout(null);

		JScrollPane reviewScrollPane = new JScrollPane();
		reviewScrollPane.setBounds(68, 65, 419, 135);
		PopUpPanel.add(reviewScrollPane);

		contentPane.add(PopUpPanel);

		final JButton btnDownload = new JButton("Download Review");
		btnDownload.setBounds(202, 212, 150, 28);
		PopUpPanel.add(btnDownload);
		btnDownload.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/download1.png")));

		final JButton btnDetails = new JButton("Show Details");
		btnDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PopUpPanel.setVisible(true);
				scrollPane.setVisible(false);
			}
		});
		btnDetails.setEnabled(false);
		btnDetails.setBounds(375, 515, 140, 41);
		btnDetails.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/show.png")));
		contentPane.add(btnDetails);

		final JButton btnDwnReview = new JButton("Recommendation");
		btnDwnReview.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (Recommendation p : myPaperListener.getConference()
						.getRecommendations())
					if (p.getPaper().equals(curPaper)) {
						myPaperListener.downloadFile(p.getFileName());
					}

			}
		});
		btnDwnReview.setEnabled(false);
		btnDwnReview.setBounds(200, 515, 155, 41);
		btnDwnReview.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/Download.png")));
		contentPane.add(btnDwnReview);

		JButton btnClose = new JButton("");
		btnClose.setBounds(513, 5, 28, 28);
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				scrollPane.setVisible(true);
				PopUpPanel.setVisible(false);
			}
		});
		btnClose.setIcon(new ImageIcon(this.getClass().getResource(
				"Icons/delet.png")));
		PopUpPanel.add(btnClose);

		final JLabel lblauthor = new JLabel("");
		lblauthor.setFont(new Font("SansSerif", Font.PLAIN, 12));
		lblauthor.setBounds(60, 43, 425, 16);
		PopUpPanel.add(lblauthor);

		JLabel lblReviewOnPaper = new JLabel("Author: ");
		lblReviewOnPaper.setBounds(19, 43, 44, 16);
		PopUpPanel.add(lblReviewOnPaper);
		lblReviewOnPaper.setFont(new Font("SansSerif", Font.PLAIN, 12));

		final JLabel lblTitle = new JLabel("Title: ");
		lblTitle.setFont(new Font("SansSerif", Font.PLAIN, 12));
		lblTitle.setBounds(19, 23, 44, 16);
		PopUpPanel.add(lblTitle);

		final JLabel lbltt = new JLabel("");
		lbltt.setFont(new Font("SansSerif", Font.PLAIN, 12));
		lbltt.setBounds(60, 23, 429, 16);
		PopUpPanel.add(lbltt);
		PopUpPanel.setVisible(false);

		//POPUP Table Section
		final DefaultTableModel myModel2 = new DefaultTableModel(
				new Object[0][0], new String[] {}) {
			private static final long serialVersionUID = 1703626494274299609L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		final JTable popUpTable = new JTable();
		reviewScrollPane.setViewportView(popUpTable);
		popUpTable.setModel(myModel2);
		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent e) {
						if (table.getValueAt(table.getSelectedRow(), 0).equals(
								PaperStatus.Recommended))
							btnDwnReview.setEnabled(true);
						else
							btnDwnReview.setEnabled(false);
						if (table.getSelectedRow() != -1) {
							for (Paper p : papers) {
								

								if (table.getValueAt(table.getSelectedRow(), 1) == p) {
									curPaper = p;
									if(p.getReviews().size() > 0)
										btnDetails.setEnabled(true);
									else 
										btnDetails.setEnabled(false);
									lbltt.setText(curPaper.getTitle());
									lblauthor.setText(curPaper.getAuthor()
											.getName());
									final Object[][] data1 = new Object[p
											.getReviews().size()][3];
									int i1 = 0;
									for (Review x : p.getReviews()) {
										data1[i1][0] = x.getReviewer()
												.getName();
										data1[i1][1] = x.getRating();
										data1[i1][2] = p.getStatus();
										i1++;
									}
									popUpTable
											.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
									myModel2.setDataVector(data1, new String[] {
											"Reviewer", "Rating", "Status" });
								}
							}
						}

					}
				});

		// Download review
		btnDownload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (Review r : curPaper.getReviews()) {
					if (popUpTable.getSelectedRow() != -1) {
						if (popUpTable.getValueAt(popUpTable.getSelectedRow(),
								0) == r.getReviewer().getName()) {
							myPaperListener.downloadFile(r.getFileName());
							System.out.println(r.getFileName());
						}
					}
				}
			}
		});

		scrollPane.setViewportView(table);
		table.getColumnModel().getColumn(0).setMaxWidth(100);
		table.getColumnModel().getColumn(0).setPreferredWidth(100);
		table.getColumnModel().getColumn(1).setPreferredWidth(300);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

	}

	/**
	 * Create ComboBox for table.
	 * 
	 * @author Jacob Trimble
	 * @author Mike Dean
	 * 
	 * @param theCb Combox to display program chairs
	 * @param theSpc list of subpcs
	 * @param thePaper the selected paper
	 */
	private static void createCB(final JComboBox<SubProgramChair> theCb,
			final List<SubProgramChair> theSpc, final Paper thePaper) {
		theCb.removeAllItems();
		theCb.setEnabled(true);

		theCb.addItem(SubProgramChair.NOT_SELECTED);
		for (int i = 0; i < theSpc.size(); i++) {
			final SubProgramChair spc = theSpc.get(i);
			if (!spc.getUser().equals(thePaper.getAuthor())
					&& (spc.getPapers().size() < 4 || spc.getUser().equals(
							thePaper.getSubProgramChair()))) {
				theCb.addItem(theSpc.get(i));
				if (spc.getUser().equals(thePaper.getSubProgramChair())) {
					theCb.setSelectedItem(spc);
					theCb.setEnabled(false);
				}
			}
		}
		theCb.invalidate();
	}

	/**
	 * Create status for comboBox
	 * 
	 * @author Jacob Trimble
	 * @author Mike Dean
	 * 
	 * @param theCb ComboBox for Status
	 * @param thePaper selected paper
	 */
	private static void createStatus(final JComboBox<PaperStatus> theCb,
			final Paper thePaper) {
		theCb.removeAllItems();
		theCb.setEnabled(true);

		theCb.addItem(PaperStatus.Submitted);
		theCb.addItem(PaperStatus.Reviewed);
		theCb.addItem(PaperStatus.Recommended);
		theCb.addItem(PaperStatus.Accepted);
		theCb.addItem(PaperStatus.Rejected);

		theCb.setSelectedItem(thePaper.getStatus());
		if (thePaper.getStatus() != PaperStatus.Recommended) {
			theCb.setEnabled(false);
		}
		theCb.invalidate();
	}

	/**
	 *  Updates the contents of the combo boxes.
	 *  
	 *  @author Mike Dean
	 */
	private void updateBoxes() {
		final List<Paper> papers = myPaperListener.getConference()
				.getSubmittedPapers();
		final List<SubProgramChair> spc = myPaperListener.getConference()
				.getSubProgramChairs();

		myDisableEvents = true;
		for (int i = 0; i < papers.size(); i++) {
			final Paper paper = papers.get(i);

			createCB(myBoxSPC.get(i), spc, paper);
			createStatus(myBoxStatus.get(i), paper);
		}
		myDisableEvents = false;
	}
}

/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.tests;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import peerReview.controller.MainController;
import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.PaperStatus;
import peerReview.model.User;

/**
 * Tests the setDecision method on the main controller.
 * 
 * @author Jacob Trimble
 */
public final class MainController_SetDecision {

	/** Controller for testing. */
	private MainController myController;
	/** Paper for testing. */
	private Paper myPaper;

	@Before
	public void setUp() {
		myController = new MainController();
		myController.login("Jacob");
		
		final User jacob = myController.getCurrentUser();
		myPaper = new Paper(new Conference("Test", new User("Jacob"), null, null),
				jacob, "Test Paper", new File("file"));
		myPaper.setStatus(PaperStatus.Recommended);
	}
	
	/** Tests a valid 'Accepted' string. */
	@Test
	public void testAccepted() {
		myController.setDecision(PaperStatus.Accepted, myPaper);
		assertEquals("Invalid decision value.", PaperStatus.Accepted, myPaper.getStatus());
	}
	/** Tests a valid 'Rejected' string. */
	@Test
	public void testRejected() {
		myController.setDecision(PaperStatus.Rejected, myPaper);
		assertEquals("Invalid decision value.", PaperStatus.Rejected, myPaper.getStatus());
	}
	/** Tests a null string. */
	@Test
	public void testNull() {
		myController.setDecision(null, myPaper);
		assertEquals("Invalid decision value.", PaperStatus.Recommended, myPaper.getStatus());
	}
	/** Tests a set twice. */
	@Test
	public void testLater() {
		myController.setDecision(PaperStatus.Accepted, myPaper);
		myController.setDecision(PaperStatus.Rejected, myPaper);
		assertEquals("Invalid decision value.", PaperStatus.Accepted, myPaper.getStatus());
	}
	/** Tests that when paper is not recommended, it will not allow change */
	@Test
	public void testNotRecommended() {
		myPaper.setStatus(PaperStatus.Submitted);
		assertEquals("Invalid decision value.", PaperStatus.Submitted, myPaper.getStatus());
		
	}
	
}

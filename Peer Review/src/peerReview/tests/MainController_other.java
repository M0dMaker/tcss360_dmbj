package peerReview.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import peerReview.controller.MainController;
import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.User;

/**
 * Test for rest of methods not covered by individual tests.
 * 
 * @author Brandon Fjellman
 *
 */
public class MainController_other {
	
	private MainController myController;
	
	private Conference myConference;
	
	@Before
	public void setUp() throws Exception {
		myController = new MainController();
		myController.login("Brandon");
		myConference = new Conference("Testing and the benefits thereof", new User("Dain"), new Date(), new Date());
		myController.chooseConference(myConference);
	}
	
	/**
	 * Test editPaper method.
	 * 
	 * @author Brandon Fjellman
	 */
	@Test
	public void testEditPaper() {
		Paper myTestPaper = new Paper(myConference, new User("Jacob"), "When I test, I write JUnit Tests!", new File("good.txt"));
		myConference.submitPaper(myTestPaper);
		String newTitle = "When I write tests, I use print statements!";
		File newFile = new File("bad.txt");
		myController.editPaper(myTestPaper, newTitle, newFile);
		
		assertTrue("The title did not change correctly.", myTestPaper.getTitle().equals(newTitle));
		assertTrue("The File did not change correctly", myTestPaper.getFileName().equals(newFile));
	}
	
	/**
	 * Test removePaper method.
	 * 
	 * @author Brandon Fjellman
	 */
	@Test
	public void testRemovePaper() {
		Paper myTestPaper = new Paper(myConference, new User("Dain"), "Test Paper", new File("test.txt"));
		myConference.submitPaper(myTestPaper);
		myController.removePaper(myTestPaper);
		
		for(Paper p : myConference.getSubmittedPapers()) {
			if(p.equals(myTestPaper)) {
				fail("The paper was found.");
			}
		}
		
		assertTrue("The paper was not found.", true);
	}

}

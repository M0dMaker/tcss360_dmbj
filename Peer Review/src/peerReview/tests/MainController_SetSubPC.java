/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.tests;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import peerReview.controller.MainController;
import peerReview.model.Conference;
import peerReview.model.SubProgramChair;
import peerReview.model.User;

/**
 * Tests the SetSubPc method on the main controller.
 * 
 * @author Mike Dean
 */
public final class MainController_SetSubPC {

	/** Controller for testing. */
	private MainController myController;
	/** Conference for testing. */
	private Conference myConference;
	
	/**
	 * @author Mike Dean
	 * @revision Brandon Fjellman
	 */
	@Before
	public void setUp() {
		myController = new MainController();
		myController.login("Jacob");
		
		myConference = new Conference("Test Conference", myController.getCurrentUser(), new Date(), new Date());
		myController.chooseConference(myConference);
	}
	
	@Test
	public void testValid() {
		final User user = new User("Test");
		myController.setSubPC(user);
		
		for (final SubProgramChair sp : myConference.getSubProgramChairs()) {
			if (sp.getUser().getName().equals(user.getName())) {
				return;
			}
		}
		
		fail("Could not find Sub-Program chair.");
	}
}

package peerReview.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.User;

/**
 * Tests the addToReview method under the User class.
 * 
 * @author Brandon Fjellman
 *
 */
public class Model_User {
	
	/** User for testing */
	private User myUser;
	/** Paper to test */
	private Paper myPaper;
	/** List to hold papers */
	private List<Paper> myList;
	/** A Conference. */
	private Conference myConference;
	
	@Before
	public void setUp() {
		myUser = new User("Brandon");
		myConference = new Conference("Test", myUser, new Date(), new Date());
		myPaper = new Paper(myConference, myUser, "Why testing is fun", new File("testingfun"));
		myList = new ArrayList<Paper>();
	}
	
	/**
	 * Checks if when a valid Paper is submitted, that it exists in the List.
	 * 
	 * Paper is created, added to review, the list is then returned and iterated through to look for the
	 * matching paper.  
	 * 
	 * @author Brandon Fjellman
	 * 
	 * Succeeds if Paper is found within the List.
	 * Fails if the Paper is not found within the list.
	 *
	 */
	@Test
	public void testAddToReviewValid() {
		
		boolean isFound = false;
		
		myUser.addToReview(myPaper);
		
		//test if added correctly
		myList = myUser.getReviews();
		
		for (Paper p : myList) {
			if (p.getTitle().equals(myPaper.getTitle())) {
				isFound = true;
				assertTrue("addToReview successful", true);
			}
		}
		 if(!isFound)
			 fail("addToReview unsuccessful");
		 
	}
	
	/**
	 * Checks if when an invalid Paper is submitted, is it not added to the List.
	 * 
	 * Creates a new Paper and a false file name to look for.  Adds the paper to the List.  Grabs the List and iterates through it,
	 * looking for the false filename.  
	 * 
	 * @author Brandon Fjellman
	 * 
	 * Succeeds:  Paper is not found.
	 * Fails:  Paper is found.
	 */
	@Test
	public void testaddToReviewInValid() {
		boolean isFound = true;
		
		Paper invalidPaper =  new Paper(myConference, myUser, "I like tests", new File("testliker"));
		String myFileName = "Tests are fun";
		
		myUser.addToReview(invalidPaper);
		
		myList = myUser.getReviews();
		
		for (Paper p : myList) {
			if (p.getFileName().equals(myFileName)) {
				fail("Invalid paper was found");
				isFound = false;
			}
		}
		if(isFound) {
			assertTrue("Invalid paper was not found", true);
		}		
		
	}
	
	/**
	 * Tests that the equals method works.
	 * 
	 * @author Jacob Trimble
	 */
	@Test
	public void testEquals() {
		final User equals = new User("Brandon");
		final User notEqual = new User("Dain");
		
		assertEquals("Papers are supposed to be equal.", equals, myUser);
		assertNotEquals("Papers are not supposed to be equal.", notEqual, myUser);
	}
	/**
	 * Tests whether the toString method works.
	 * 
	 * @autor Jacob Trimble
	 */
	@Test
	public void testToString() {
		assertEquals("toString does not work.", "Brandon", myUser.toString());
	}

}

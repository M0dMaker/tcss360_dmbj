/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import peerReview.controller.MainController;
import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.Review;
import peerReview.model.User;

/**
  * Tests the set submit review method on the main controller.
 * 
 * @author Dain Landis
 */
public class MainController_SubmitReview {

	/**
	 * Controller for testing.
	 */
	private MainController myController;
	
	/**
	 * Conference for testing.
	 */
	private Conference myConference;
	
	@Before
	public void setUp() throws Exception {
		myController = new MainController();
		
		// set as current user
		myController.login("Jacob");
		
		myConference = new Conference("My Conference", new User("Mike"), new Date(), new Date());
		myController.chooseConference(myConference);		
	}

	/**
	 * Testing VALID submitReview to make sure the review submits.
	 * 
	 * @author Dain Landis
	 * 
	 * Created a paper, submitted the paper, grabbed the paper from collection 
	 * to submit the review to, submitted the review, grabbed the paper collection 
	 * again to check whether the paper now had the review.
	 * Fails on null testPaper return
	 * Fails on an valid paper being returned as invalid.
	 * Passes on valid paper being found.
	 */
	@Test
	public void submitValidReviewTest() {
		//hold the paper
		Paper testPaper = null;
		
		//create user to set as current user
		myController.login("Jacob");
		
		//create a review name
		final File reviewFileName = new File("REVIEW OF DAIN");
		
		//submit a paper to conference to add review to
		myController.submitPaper(new File("The Filename"), "The Title");
		
		List<Paper> thePapers = myController.getConference().getSubmittedPapers();
				
		for (Paper p : thePapers) {
			if (p.getFileName().equals(new File("The Filename"))) {
				testPaper = p;
			}
		}
		if (testPaper == null) {
			fail("Paper not found");
		}

		//test method
		myController.submitReview(testPaper, reviewFileName, 1);

		//get the list of papers again to find the paper with the review
		thePapers = myController.getConference().getSubmittedPapers();
		for (Paper p : thePapers) {
			if (p.getFileName().equals("Dains Paper")) {
				testPaper = p;
			}
		}
		if (testPaper == null) {
			fail("Paper not found");
		}
		
		//boolean to hold if review was found
		boolean isFound = false;
		
		List<Review> theReviews = testPaper.getReviews();
		for (Review r : theReviews) {
			if (r.getFileName().equals(reviewFileName)) {
				assertTrue("Review submission successful", true);
				isFound = true;
			}
		}
		
		if (!isFound) {
			fail("Review submission failed... :");
		}
	}
	
	/**
	 * Testing INVALID submitReview for not found reviews.
	 * 
	 * @author Dain Landis
	 * 
	 * Created a paper, submitted the paper, grabbed the paper from 
	 * collection to submit the review to, submitted an invalid review, grabbed the 
	 * paper collection again to check whether the paper now had the review.
	 * Fails on null testPaper return
	 * Fails on an invalid paper being returned as valid.
	 * Passes on fail for a valid paper 
	 */
	@Test
	public void submitInvalidReviewTest() {
		//hold the paper
		Paper testPaper = null;
		
		//create user to set as current user
		myController.login("Mike");
		
		//create a review name
		final File reviewFileName = new File("REVIEW OF MIKE");
		
		//submit a paper to conference to add review to
		myController.submitPaper(new File("File"), "The File");
		
		List<Paper> thePapers = myController.getConference().getSubmittedPapers();
		
		for (Paper p : thePapers) {
			if (p.getFileName().equals(new File("File"))) {
				testPaper = p;
			}
		}
		if (testPaper == null) {
			fail("Paper not found");
		}

		//test method
		myController.submitReview(testPaper, reviewFileName, 1);

		//get the list of papers again to find the paper with the review
		thePapers = myController.getConference().getSubmittedPapers();
		for (Paper p : thePapers) {
			if (p.getFileName().equals("Dains Paper")) {
				testPaper = p;
			}
		}
		if (testPaper == null) {
			fail("Paper not found");
		}
		
		//boolean to hold if review was found
		boolean isFound = false;
		
		List<Review> theReviews = testPaper.getReviews();
		for (Review r : theReviews) {
			if (r.getFileName().equals("Invalid Paper")) {
				fail("Review submission failed... Returned true"
						+ " for invalid review :");
				isFound = true;
			}
		}
		
		if (!isFound) {
			assertTrue("Review submission passed... returned false"
					+ " for invalid review", true);
		}
	}
}

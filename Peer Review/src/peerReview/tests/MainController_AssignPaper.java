/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import peerReview.controller.MainController;
import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.User;

/**
 * Tests the assignPaper method on the main controller.
 * 
 * @author Dain Landis
 */
public final class MainController_AssignPaper {

	/** Controller for testing. */
	private MainController myController;
	
	/** Conference for testing */
	private Conference myConference;
	
	/**
	 * @author Dain Landis
	 * @revision Brandon Fjellman
	 */
	@Before
	public void setUp() {
		myController = new MainController();
		myController.login("Jacob");
		myConference = new Conference("Name", new User("Mike"), new Date(), new Date());
		myController.chooseConference(myConference);
	}

	/**
	 * Testing assign paper with valid arguments.
	 * 
	 * @author Dain Landis
	 */
	@Test
	public void testValid() {
		final Paper paper = new Paper(myConference, new User("Jacob"), "Test Paper", new File("Filename.dat"));
		final User user = new User("Dain");
		
		assertEquals("Invalid paper submission.", 0, myController.assignPaper(paper, user));
	}

	/**
	 * Testing assign paper with the author is the reviewer.
	 * 
	 * @author Dain Landis
	 */
	@Test
	public void testBadAuthor() {
		final User user = new User("Dain");
		final Paper paper = new Paper(myConference, user, "Test Paper", new File("Filename.dat"));
		
		assertEquals("Invalid paper submission.", 1, myController.assignPaper(paper, user));
	}

	/**
	 * Testing assign paper with the author is the reviewer.
	 * 
	 * @author Dain Landis
	 */
	@Test
	public void testTooMany() {
		final Paper paper1 = new Paper(myConference, new User("Jacob"), "Test Paper 1", new File("Filename1.dat"));
		final Paper paper2 = new Paper(myConference, new User("Jacob"), "Test Paper 2", new File("Filename2.dat"));
		final Paper paper3 = new Paper(myConference, new User("Jacob"), "Test Paper 3", new File("Filename3.dat"));
		final Paper paper4 = new Paper(myConference, new User("Jacob"), "Test Paper 4", new File("Filename4.dat"));
		final Paper paper5 = new Paper(myConference, new User("Jacob"), "Test Paper 5", new File("Filename5.dat"));
		final User user = new User("Dain");
		
		assertEquals("Invalid paper submission.", 0, myController.assignPaper(paper1, user));
		assertEquals("Invalid paper submission.", 0, myController.assignPaper(paper2, user));
		assertEquals("Invalid paper submission.", 0, myController.assignPaper(paper3, user));
		assertEquals("Invalid paper submission.", 0, myController.assignPaper(paper4, user));

		assertEquals("Invalid paper submission.", 2, myController.assignPaper(paper5, user));
	}
	
}

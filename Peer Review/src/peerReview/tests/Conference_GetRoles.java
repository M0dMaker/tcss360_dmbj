/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.SubProgramChair;
import peerReview.model.User;

/**
 * Test class for the getRoles method on the conference.
 * 
 * @author Brandon Fjellman
 */
public final class Conference_GetRoles {
	
	/** User for testing. */
	private User myUser;	

	/** Conference for testing. */
	private Conference myConference;

	@Before
	public void setUp() {
		myUser = new User("Jacob");		
		myConference = new Conference("A conference", myUser, null, null);
	}

	/**
	 * Tests the case that has a program chair.
	 * 
	 * @author Brandon Fjellman
	 * @revision Jacob Trimble
	 */
	@Test
	public void testProgramChair() {
		final List<String> roles = myConference.getRoles(myUser);
		assertTrue("Program chair not found.", roles.contains("Program Chair"));
	}
	/**
	 * Tests the case that does not have a program chair.
	 * 
	 * @author Brandon Fjellman
	 * @revision Jacob Trimble
	 */
	@Test
	public void testNoProgramChair() {
		final List<String> roles = myConference.getRoles(new User("Dain"));
		assertFalse("Program chair found.", roles.contains("Program Chair"));
	}
	/**
	 * Tests the case that has a subprogram chair.
	 * 
	 * @author Brandon Fjellman
	 * @revision Jacob Trimble
	 */
	@Test
	public void testSubProgramChair() {
		final User dain = new User("Dain");
		SubProgramChair mySubPC = new SubProgramChair(dain);
		myConference.addSubProgramChair(mySubPC);

		final List<String> roles = myConference.getRoles(dain);
		assertTrue("SubProgram chair not found.", roles.contains("SubProgram Chair"));
	}
	/**
	 * Tests the case that does not have a subprogram chair.
	 * 
	 * @author Brandon Fjellman
	 * @revision Jacob Trimble
	 */
	@Test
	public void testNoSubProgramChair() {
		final List<String> roles = myConference.getRoles(new User("Mike"));
		assertFalse("SubProgramChair found.", roles.contains("SubProgram Chair"));
	}
	/**
	 * Tests the case that has an author.
	 * 
	 * @author Brandon Fjellman
	 * @revision Jacob Trimble
	 */
	@Test
	public void testAuthor() {
		User mike = new User("Mike");
		final Paper myPaper = new Paper(null, mike, "Mikes also loves testing.", new File("theFileName"));
		myConference.submitPaper(myPaper);
		
		final List<String> roles = myConference.getRoles(mike);
		assertTrue("Author not found.", roles.contains("Author"));
	}
	
	/**
	 * Tests the case that has a Reviewer.
	 * 
	 * @author Brandon Fjellman
	 * @revision Jacob Trimble
	 */
	@Test
	public void testReviewer() {
		final User myReviewer = new User("Mike");
		Paper reviewpaper = new Paper(myConference, new User("Dain"), "This paper isn't any good, review it!", new File("thefilename"));
		myReviewer.addToReview(reviewpaper);
		
		final List<String> roles = myConference.getRoles(myReviewer);
		assertTrue("Reviewer not found.", roles.contains("Reviewer"));
	}
	/**
	 * Tests the case that has no Reviewer
	 * 
	 * @author Brandon Fjellman
	 * @revision Jacob Trimble
	 */
	@Test
	public void testNoReviewer() {
		final List<String> roles = myConference.getRoles(new User ("Jacob"));
		assertFalse("Reviewer found.", roles.contains("Reviwer"));
	}

}

package peerReview.tests;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import peerReview.model.Review;
import peerReview.model.User;

/**
 * Tests for Review Class
 * 
 * @author Brandon Fjellman
 */
public class Model_Review {
	
	/** Create a user for testing. */
	private User myUser;
	/** Create a file for testing */
	private File myFile;
	/** Create a Review for testing */
	private Review myReview;
	/** Create an int to use as a rating */
	private int rating;

	@Before
	public void setUp() {
		myUser = new User("Dain");
		myFile = new File("filename");
		rating = 3;
		myReview = new Review(myUser, myFile, rating);
	}
	
	/**
	 * Test Review Constructor.
	 * 
	 * @author Brandon Fjellman
	 * 
	 * Passes if all elements retrieved are what was passed.
	 * Fails if elements retrieved are different from what was passed.
	 */
	@Test
	public void testReviewConstructor() {
		
		if(myReview.getFileName().getName().equals(myFile.getName())) {
			assertTrue("File name found in Constructor.", true);
		} else {
			fail("File name not found in Constructor.");
		}
		
		if(myReview.getRating() == rating) {
			assertTrue("Rating passes.", true);
		} else {
			fail("Rating fails.");
		}
		
		if(myReview.getReviewer().getName().equals(myUser.getName())) {
			assertTrue("User found in Constructor.", true);
		} else {
			fail("User not found in Constructor.");
		}
		
	}
	
	/**
	 * Test setRating method in Review class.
	 * 
	 * @author Brandon Fjellman
	 * 
	 * Passes if only values between 1 and 5 are accepted.
	 * Fails if values under 1 and over 5 are allowed.
	 */
	@Test
	public void testSetRating() {
		
		//test set higher
		myReview.setRating(6);
		assertTrue("Rating set too high passed.  FAIL!", rating == myReview.getRating());
		
		//test set lower
		assertTrue("Rating set too low passed.  FAIL!", rating == myReview.getRating());
		
		//test just right
		rating = 1;
		myReview.setRating(rating);
		assertTrue("Rating in proper range failed.", rating == myReview.getRating());		
	}
	
	/**
	 * Tests that the equals method works.
	 * 
	 * @author Jacob Trimble
	 */
	@Test
	public void testEquals() {
		final Review equals = new Review(myUser, myFile, rating);
		final Review notEqual = new Review(myUser, new File("Test2"), rating);
		
		assertEquals("Reviews are supposed to be equal.", equals, myReview);
		assertNotEquals("Reviews are not supposed to be equal.", notEqual, myReview);
	}

}

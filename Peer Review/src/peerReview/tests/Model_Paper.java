package peerReview.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.PaperStatus;
import peerReview.model.Review;
import peerReview.model.User;

/**
 * Test all methods of the Paper Class
 * 
 * @author Dain Landis
 */
public class Model_Paper {

	/** a Paper */
	private Paper myPaper;
	/** Contains a conference for testing. */
	private Conference myConference;
	
	/**
	 * Setup for Paper constructor
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		myConference = new Conference("Test", new User("Dain"), null, null);
		myPaper = new Paper(myConference, new User("Dain"), "A good Paper on Ethics", new File("testingFile.pdf"));		
	}

	/**
	 * Testing the paper constructor for valid and invalid data
	 * 
	 * @author Dain Landis
	 * 
	 * Passes if each declaration matches.
	 */
	@Test
	public void testValidPaperConstructor() {
		assertTrue("Error verifying valid paper author", 
				myPaper.getAuthor().getName().toString().equals("Dain"));	
		assertTrue("Error verifying valid paper Title", 
				myPaper.getTitle().toString().equals("A good Paper on Ethics"));	
		assertTrue("Error verifying valid paper File", 
				myPaper.getFileName().toString().equals("testingFile.pdf"));
		assertTrue("Error verifying valid paper Status", 
				myPaper.getStatus().equals(PaperStatus.Submitted));
		assertFalse("Error verifying invalid author", 
				myPaper.getAuthor().getName().toString().equals("Bob"));	
		assertFalse("Error verifying invalid Title", 
				myPaper.getTitle().toString().equals("A bad Paper on Ethics"));
		assertFalse("Error verifying invalid File", 
				myPaper.getFileName().toString().equals("testingBADFile.pdf"));
		assertFalse("Error verifying invalid paper Status", 
				myPaper.getStatus().equals(PaperStatus.Rejected));
		
		if(myPaper.getSubProgramChair() != null) {
			fail("Error verifying null paper SubPC");
		}
	}
	
	/**
	 * Tests the Paper class setStatus
	 * Checks for all possible paper status
	 * Accept, Standby, Rejected
	 * 
	 * @author Dain Landis
	 */
	@Test
	public void testPaperSetStatus(){
		myPaper.setStatus(PaperStatus.Accepted);
		assertTrue("Error verifying valid paper setStatus Accepted", 
				myPaper.getStatus().equals(PaperStatus.Accepted));
		myPaper.setStatus(PaperStatus.Submitted);
		assertTrue("Error verifying valid paper setStatus Standby", 
				myPaper.getStatus().equals(PaperStatus.Submitted));
		myPaper.setStatus(PaperStatus.Rejected);
		assertTrue("Error verifying valid paper setStatus Rejected",
				myPaper.getStatus().equals(PaperStatus.Rejected));		
	}
	
	/**
	 * Tests the Paper class setSubProgramChair
	 * Checks for both a valid user setting and invalid.
	 * 
	 * @author Dain Landis
	 */
	@Test
	public void testSetSubProgramChair(){
		myPaper.setSubProgramChair(new User("A Awesome SubProgram Chair"));
		assertTrue("Error verifying valid paper setSubProgramChair", 
				myPaper.getSubProgramChair().getName().equals("A Awesome SubProgram Chair"));
		assertFalse("Error verifying invalid paper setSubProgramChair", 
				myPaper.getSubProgramChair().getName().equals("A NOT Awesome SubProgram Chair"));
	}
	
	/**
	 * Tests the Paper class submitReview
	 * Verifies that the review has been added to the list
	 * If list is NULL return error
	 * If review is not found, fail
	 * 
	 * @author Dain Landis
	 */
	@Test
	public void testSubmitReview(){
		myPaper.submitReview(new Review(new User("Bobby"), new File("reviewFile.pdf"), 10));	
		boolean foundReview = false;
		List<Review> testReviews = myPaper.getReviews();
		
		if(testReviews == null) {
			fail("Error verifying testReviews | returning null");
		}
		
		for(Review r : testReviews){
			if(r.getFileName().toString().equals("reviewFile.pdf") &&
					r.getReviewer().getName().toString().equals("Bobby") &&
					r.getRating() == 10) {
				foundReview = true;
			}
		}
		if(!foundReview){
			fail("Error verifying testReviews | review not found");
		}		
	}
	
	/**
	 * Test that setTitle sets the title properly.
	 * 
	 * @author Brandon Fjellman
	 */
	@Test
	public void testSetTitle() {
		final Paper titleTester = new Paper(myConference, new User("Mike"), "Writing tests make me happy", new File("coolfilename"));
		String testTitle = "Strings are happier than integers";
		titleTester.setTitle(testTitle);
		
		assertTrue("Title was not changed successfully.", titleTester.getTitle().equals(testTitle));
	}
	
	/**
	 * Test that setFile sets the new File properly.
	 * 
	 * @author Brandon Fjellman
	 */
	@Test
	public void testSetFile() {
		final Paper fileTester = new Paper(myConference, new User("Jacob"), "I like files", new File("bestfilename.txt"));
		File testFile = new File("worsefilename.txt");
		fileTester.setFile(testFile);
		
		assertTrue("File was not changed successfully.", fileTester.getFileName().getName().equals(testFile.getName()));
	}
	
	/**
	 * Tests that the equals method works.
	 * 
	 * @author Jacob Trimble
	 */
	@Test
	public void testEquals() {
		final Paper equalPaper = new Paper(myConference, new User("Dain"), "A good Paper on Ethics", new File("testingFile.pdf"));
		final Paper notEqual = new Paper(myConference, new User("Dain"), "Another paper", new File("file"));
		
		assertEquals("Papers are supposed to be equal.", equalPaper, myPaper);
		assertNotEquals("Papers are not supposed to be equal.", notEqual, myPaper);
	}
	/**
	 * Tests whether the toString method works.
	 * 
	 * @autor Jacob Trimble
	 */
	@Test
	public void testToString() {
		assertEquals("toString does not work.", "A good Paper on Ethics", myPaper.toString());
	}
}
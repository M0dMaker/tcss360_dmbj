/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import peerReview.controller.MainController;
import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.User;

/**
 * Tests the getUsersPapers method on the main controller.
 * 
 * @author Jacob Trimble
 */
public final class MainController_GetUserPapers {

	/** Controller for testing. */
	private MainController myController;
	

	@Before
	public void setUp() {
		myController = new MainController();
		
		myController.login("Jacob");
		final User jacob = myController.getCurrentUser();
		
		myController.login("Mike");
		final User mike = myController.getCurrentUser();
		
		final Conference conf = new Conference("A Conference", jacob, 
				new GregorianCalendar(2015, 1, 1).getTime(), new GregorianCalendar(2015, 2, 1).getTime());
		myController.chooseConference(conf);

		final Paper paper1 = new Paper(conf, jacob, "Test paper", new File("paper1"));
		final Paper paper2 = new Paper(conf, jacob, "Test paper Also", new File("paper2"));
		final Paper paper3 = new Paper(conf, mike, "Test paper Again", new File("paper3"));
		
		conf.submitPaper(paper1);
		conf.submitPaper(paper2);
		conf.submitPaper(paper3);
	}

	/**
	 * Testing get user papers with two results.
	 * 
	 * @author Jacob Trimble
	 */
	@Test
	public void testTwoPapers() {
		myController.login("Jacob");
		final List<Paper> papers = myController.getUserPapers();
		assertEquals("Two papers expected.", 2, papers.size());
	}
	/**
	 * Testing get user papers with one results.
	 * 
	 * @author Jacob Trimble
	 */
	@Test
	public void testOnePapers() {
		myController.login("Mike");
		final List<Paper> papers = myController.getUserPapers();
		assertEquals("One papers expected.", 1, papers.size());
	}
	/**
	 * Testing get user papers with one results.
	 * 
	 * @author Jacob Trimble
	 */
	@Test
	public void testZeroPapers() {
		myController.login("Brandon");
		final List<Paper> papers = myController.getUserPapers();
		assertEquals("Zero papers expected.", 0, papers.size());
	}
	
}

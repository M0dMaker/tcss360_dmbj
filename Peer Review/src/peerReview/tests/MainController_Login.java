/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import peerReview.controller.MainController;

/**
 * Test the login method in the Controller class.
 * 
 * @author Brandon Fjellman
 */
public class MainController_Login {

	/** Controller for testing. */
	private MainController myController;
	
	@Before
	public void setUp() throws Exception {		
		myController = new MainController();
	}

	/**
	 * Testing valid login for if a user exited to login or not.
	 * 
	 * @author Brandon Fjellman
	 * 
	 * Fails if Mike is not found
	 * Passes if mike is found
	 */
	@Test
	public void testValidLogon() {
		assertTrue("Mike is a user", myController.login("Mike"));		
	}
	
	/**
	 * Testing invalid login for if a user exited to login or not.
	 * 
	 * @author Brandon Fjellman
	 * 
	 * Fails if Otis is found
	 * Passes if Otis is NOT found
	 */
	@Test
	public void testInvalidLogon() {
		assertFalse("Otis is not a user", myController.login("Otis"));
	}

	/**
	 * Testing logged out with user.
	 * 
	 * @author Brandon Fjellman
	 * 
	 * Fails if still logged in.
	 * Passes if not logged in.
	 */
	@Test
	public void testInLogout() {
		myController.login("Jacob");
		
		myController.logout();
		assertNull("Still logged in.", myController.getCurrentUser());
	}
	/**
	 * Testing logged out without user.
	 * 
	 * @author Brandon Fjellman
	 * 
	 * Fails if still logged in.
	 * Passes if not logged in.
	 */
	@Test
	public void testOutLogout() {		
		myController.logout();
		assertNull("Still logged in.", myController.getCurrentUser());
	}
}



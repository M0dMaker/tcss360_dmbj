package peerReview.tests;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.SubProgramChair;
import peerReview.model.User;

/**
 * Tests for SubProgramChair class.
 * 
 * @author Brandon Fjellman
 *
 */
public class Model_SubProgramChair {
	
	/** User for testing */
	private User myUser;
	/** User to add as SubProgramChair */
	private User mySubUser;
	/** SubProgram Chair for testing */
	private SubProgramChair mySPC;
	/** Paper to test */
	private Paper myPaper;
	
	@Before
	public void setUp() {
		myUser = new User("Brandon");
		mySubUser = new User("Mike");
		mySPC = new SubProgramChair(mySubUser);
		myPaper = new Paper(new Conference("Test", new User("Dain"), null, null),
				myUser, "Why testing is fun", new File("testingfun"));
	}
	
	/**
	 * Testing the SubProgramChair constructor.
	 * 
	 * @author Brandon Fjellman
	 * 
	 * Passes if the User parameter used when constructing object is found.
	 * Fails if the User parameter used when constructing object is not found.
	 */
	@Test 
	public void testSubProgramChairConstructor() {
		if(mySPC.getUser().equals(mySubUser)) {
			assertTrue("SubProgramChair constructor works", true);
		} else {
			fail("SubProgramChair constructor failed.");
		}
	}
	
	/**
	 * Testing the addPaper() method of the SubProgram Chair class.
	 * 
	 * @author Brandon Fjellman
	 * 
	 * Passes if the Paper is found in the Paper list.
	 * Fails if the Paper is not found in the Paper list.
	 */
	@Test
	public void testAddPaper() {
		
		mySPC.addPaper(myPaper);
		boolean located = false;
		
		for (Paper p : mySPC.getPapers()) {
			if(p.getTitle().equals(myPaper.getTitle())) {
				assertTrue("Paper successfully added.", true);
				located = true;
			}
		}
		
		if (!located) 
			fail("Paper was not found.");
	}
	
	/**
	 * Testing the addReviewer() method of the SubProgramChair class.
	 * 
	 * @author Brandon Fjellman
	 * 
	 * Passes if the User is found in the reviewer list.
	 * Fails if the User is not found in the reviewer list.
	 */
	@Test
	public void testAddReviewer() {
		
		mySPC.addReviewer(myUser);
		boolean located = false;
		
		for (User u : mySPC.getReviewers()) {
			if(u.getName().equals(myUser.getName())) {
				assertTrue("Reviewer succesfully added.", true);
				located = true;
			}
		}
		
		if(!located)
			fail("Reviwer was not found.");
	}
	
	/**
	 * Tests that the equals method works.
	 * 
	 * @author Jacob Trimble
	 */
	@Test
	public void testEquals() {
		final SubProgramChair equals = new SubProgramChair(mySubUser);
		final SubProgramChair notEqual = new SubProgramChair(new User("Test"));
		
		assertEquals("Papers are supposed to be equal.", equals, mySPC);
		assertNotEquals("Papers are not supposed to be equal.", notEqual, mySPC);
	}
	/**
	 * Tests whether the toString method works.
	 * 
	 * @author Jacob Trimble
	 */
	@Test
	public void testToString() {
		assertEquals("toString does not work.", "Mike", mySPC.toString());
	}
}

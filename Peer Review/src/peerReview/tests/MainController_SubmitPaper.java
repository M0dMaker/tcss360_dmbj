/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import peerReview.controller.MainController;
import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.User;

/**
 * Tests the set submit paper method on the main controller.
 * 
 * @author Mike Dean
 */
public class MainController_SubmitPaper {
	
	/**
	 * Controller for testing.
	 */
	private MainController myController;
	
	/**
	 * Conference for testing.
	 */
	private Conference myConference;
	
	/**
	 * @author Mike Dean
	 * @revision Dain Landis
	 * @revision Brandon Fjellman 
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {

		myController = new MainController();
		myController.login("Jacob");
		myConference = new Conference("Conference", new User("Mike"), new Date(), new Date());
		myController.chooseConference(myConference);

	}

	/**
	 * Testing submitPaper for submitting papers to a conference.
	 * Submitted a paper, grabbed the collection of papers and make sure it exists.
	 * 
	 * @author Mike Dean
	 * @revision Brandon Fjellman
	 * @revision Dain Landis
	 * 
	 * Fails on paper not found
	 * Passes if paper is found
	 */
	@Test
	public void submitValidPaperTest() {

		//boolean to show if paper was found
		boolean paperFound = false;
				
		//submit a paper to conference to add review to
		myController.submitPaper(new File("Test"), "Dains Paper");
		
		//grab paper list from conference
		List<Paper> testPaperList = myController.getConference().getSubmittedPapers();
				
		//look for paper in list
		for (Paper p : testPaperList) {
						
			if (p.getFileName().equals(new File("Test"))) {
				assertTrue("Paper found", true);
				paperFound = true;
			}	
		}
		//paper was not found, fail
		if(!paperFound) {
			fail("Paper not found");
		}		
	}
	
	/**
	 * Testing submitPaper to make sure that it doesn't return a Paper that doesn't exist.
	 * Submitted a paper, and check the collection for a paper that does not exist.
	 * 
	 * @author Mike Dean
	 * @revision Brandon Fjellman
	 * @revision Dain Landis
	 * 
	 * Fails if an invalid paper is returned
	 * Passes if the paper is not found
	 */
	@Test
	public void submitInvalidPaperTest() {
		//boolean to show if paper was found
		boolean paperFound = false;		
		
		//submit a paper to conference to add review to
		myController.submitPaper(new File("Test File"), "Dains Paper");
		
		//grab paper list from conference
		List<Paper> testPaperList = myController.getConference().getSubmittedPapers();
				
		//look for paper in list
		for (Paper p : testPaperList) {
			
			if (p.getFileName().equals(new File("Test"))) {
				fail("Paper found");
				paperFound = true;
			}//end if	
		}//end for
		//paper was not found, success!
		if (!paperFound) {
			assertTrue("Paper not found", true);		
		}
	}
}

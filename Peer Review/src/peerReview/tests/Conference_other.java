/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;

import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.Recommendation;
import peerReview.model.SubProgramChair;
import peerReview.model.User;

/**
 * Tests other methods on the conference.
 * 
 * @author Jacob Trimble
 */
public final class Conference_other {

	/** Conference for testing. */
	private Conference myConference;
	/** A user for testing. */
	private User myUser;

	@Before
	public void setUp() {
		myUser = new User("Jacob");		
		myConference = new Conference("A conference", myUser, 
				new GregorianCalendar(2015, 1, 1).getTime(), 
				new GregorianCalendar(2015, 2, 1).getTime());
	}

	/**
	 * Tests the constructor
	 * 
	 * @author Jacob Trimble
	 */
	@Test
	public void constructorTest() {		
		assertEquals("Invalid title.", "A conference", myConference.getName());
		assertEquals("Invalid program chair.", myUser, myConference.getProgramChair());
		assertEquals("Invalid submission date.", new GregorianCalendar(2015, 1, 1).getTime(), myConference.getSubmissionDate());
		assertEquals("Invalid review date.", new GregorianCalendar(2015, 2, 1).getTime(), myConference.getReviewDate());
	}
	/**
	 * Tests the submitRecommendation method.
	 * 
	 * @author Jacob Trimble
	 */
	@Test
	public void submitRecommendationTest() {
		final SubProgramChair spc = new SubProgramChair(myUser);
		final Paper paper = new Paper(myConference, myUser, "Title", new File("File"));
		final Recommendation rec = new Recommendation(spc, paper, new File("FileName"));
		
		myConference.submitRecommendation(rec);
		assertTrue("Cannot find recommendation.", myConference.getRecommendations().contains(rec));
	}
	/**
	 * Tests the submitPaper method
	 * 
	 * @author Brandon Fjellman
	 */
	@Test
	public void submitPaperTest() {
		final Paper paper = new Paper(myConference, new User("Jacob"), "I love testing", new File("filename"));
		
		myConference.submitPaper(paper);
		assertTrue("Cannot find the Paper.", myConference.getSubmittedPapers().contains(paper));
	}
	/**
	 * Tests the addSubProgramChair method.
	 * 
	 * @author Brandon Fjellman
	 */
	@Test
	public void addSubProgramChairTest() {
		final SubProgramChair spc = new SubProgramChair(myUser);
		
		myConference.addSubProgramChair(spc);
		assertTrue("Cannot find sub-program chair.", myConference.getSubProgramChairs().contains(spc));
	}
}

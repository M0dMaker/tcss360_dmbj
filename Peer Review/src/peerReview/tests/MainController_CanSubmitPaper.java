/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.tests;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;

import peerReview.controller.MainController;
import peerReview.model.Conference;
import peerReview.model.User;

/**
 * Tests the canSubmitPaper method on the main controller.
 * 
 * @author Jacob Trimble
 */
public class MainController_CanSubmitPaper {

	/** Controller for testing. */
	private MainController myController;
	
	@Before
	public void setUp() {
		myController = new MainController();
		myController.login("Jacob");
	}

	@Test
	public void testValid() {
		test(new GregorianCalendar(2015, 1, 1).getTime(), 
			 new GregorianCalendar(2015, 1, 1).getTime(),
			 true);
	}
	@Test
	public void testInvalid() {
		test(new GregorianCalendar(2014, 1, 1).getTime(), 
				new GregorianCalendar(2014, 1, 1).getTime(),
				false);
	}
	@Test
	public void testBetween() {
		test(new GregorianCalendar(2014, 1, 1).getTime(), 
				new GregorianCalendar(2015, 1, 1).getTime(),
				false);
	}
	
	private void test(final Date theDate1, final Date theDate2, final boolean theExpected) {
		final Conference conf = new Conference("Test Conference", new User("Jacob"), 
				theDate1, theDate2);
		
		myController.chooseConference(conf);
		assertEquals("Invalid can submit paper value.", theExpected, myController.canSubmitPaper());		
	}

}

/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.tests;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.GregorianCalendar;
import org.junit.Before;
import org.junit.Test;

import peerReview.controller.MainController;
import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.User;

/**
 * Tests the getReviews method on the main controller.
 * 
 * @author Dain Landis
 */
public final class MainController_GetReviews {

	/** Controller for testing. */
	private MainController myController;	
	/** Paper holder for primary conference. */
	private Paper myPaper1;
	/** Paper holder for primary conference. */
	private Paper myPaper2;
	/** Paper holder for primary conference. */
	private Paper myPaper3;	
	/** Paper holder for secondary conference. */
	private Paper myPaper4;
	/** Paper holder for secondary conference. */
	private Paper myPaper5;
	/** Paper holder for secondary conference. */
	private Paper myPaper6;	
	/** A user to test with on both conferences. */
	private User jacob;
	/** A user to test with on both conferences. */
	private User mike;
	/** A user to test with on both conferences. */
	private User brandon;
	/** A primary conference to test against. */
	private Conference primaryConf;
	/** A secondary conference to test against. */
	private Conference secondConf;

	/**
	 * Setup both conferences and users for testing.
	 * Will add users, papers, and reviews
	 * 
	 * @author Dain Landis
	 */
	@Before
	public void setUp() {
		myController = new MainController();
		
		myController.login("Jacob");
		jacob = myController.getCurrentUser();
		
		myController.login("Mike");
		mike = myController.getCurrentUser();
		
		myController.login("Brandon");
		brandon = myController.getCurrentUser();
		
		primaryConf = new Conference("A Primary Conference", mike, 
				new GregorianCalendar(2015, 1, 1).getTime(), new GregorianCalendar(2015, 2, 1).getTime());
		
		secondConf = new Conference("A Second Conference", brandon, 
				new GregorianCalendar(2015, 1, 1).getTime(), new GregorianCalendar(2015, 2, 1).getTime());
		
		myPaper1 = new Paper(primaryConf, jacob, "Test paper", new File("paper1"));
		myPaper2 = new Paper(primaryConf, jacob, "Test paper Also", new File("paper2"));
		myPaper3 = new Paper(primaryConf, jacob, "Test paper Again", new File("paper3"));
		myPaper4 = new Paper(secondConf, jacob, "Test paper 2", new File("paper4"));
		myPaper5 = new Paper(secondConf, jacob, "Test paper Also 2", new File("paper5"));
		myPaper6 = new Paper(secondConf, jacob, "Test paper Again 2", new File("paper6"));
	}
	
	/**
	 * Testing getReviews method returning proper review count
	 * for given user on primary conference.
	 * 
	 * @author Dain Landis
	 * 
	 * Fails if 2 reviews are not found.
	 * Passes if 2 reviews are found.
	 */
	@Test
	public void testHasReviewsPrimaryConf() {
		myController.chooseConference(primaryConf);
		primaryConf.submitPaper(myPaper1);
		primaryConf.submitPaper(myPaper2);
		primaryConf.submitPaper(myPaper3);		
		mike.addToReview(myPaper1);
		mike.addToReview(myPaper2);
		assertTrue("Error user mike has wrong review count", myController.getReviews(mike) != null 
				&& myController.getReviews(mike).size() == 2);
	}
	
	/**
	 * Testing getReviews method returning proper review count
	 * for given user on second conference.
	 * 
	 * @author Dain Landis
	 * 
	 * Fails if 3 reviews are not found.
	 * Passes if 3 reviews are found.
	 */
	@Test
	public void testHasReviewsSecondConf() {
		myController.chooseConference(secondConf);
		secondConf.submitPaper(myPaper4);
		secondConf.submitPaper(myPaper5);
		secondConf.submitPaper(myPaper6);		
		brandon.addToReview(myPaper4);
		brandon.addToReview(myPaper5);
		brandon.addToReview(myPaper6);		
		assertTrue("Error user brandon has wrong review count", myController.getReviews(brandon) != null 
				&& myController.getReviews(brandon).size() == 3);
	}
	
	/**
	 * Tests that reviews do not cross conferences
	 * 
	 * @author Dain Landis
	 * 
	 * Fails if reviews are seen in another conference.
	 * Passes if reviews are not seen in another conference.
	 */
	@Test
	public void testCrossConferenceReviews() {
		myController.chooseConference(primaryConf);
		primaryConf.submitPaper(myPaper1);
		primaryConf.submitPaper(myPaper2);
		primaryConf.submitPaper(myPaper3);		
		mike.addToReview(myPaper1);
		mike.addToReview(myPaper2);
		assertTrue("Error user Mike failed on reviews in primary conference",
				myController.getReviews(mike) != null 
				&& myController.getReviews(mike).size() == 2);		
		myController.chooseConference(secondConf);		
		assertTrue("Error user Mike failed on reviews in second conference",
				myController.getReviews(mike) != null 
				&& myController.getReviews(mike).size() == 0);
	}
}

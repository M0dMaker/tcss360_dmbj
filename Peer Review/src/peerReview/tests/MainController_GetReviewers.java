/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.tests;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import peerReview.controller.MainController;
import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.User;

/**
 * Tests the getReviewers method on the main controller.
 * 
 * @author Jacob Trimble
 */
public final class MainController_GetReviewers {

	/** Controller for testing. */
	private MainController myController;
	/** Contains a paper with two reviewers (mike and dain). */
	private Paper myPaper1;
	/** Contains a paper with one reviewers (mike). */
	private Paper myPaper2;
	/** Contains a paper with zero reviewers. */
	private Paper myPaper3;

	@Before
	public void setUp() {
		myController = new MainController();
		
		myController.login("Jacob");
		final User jacob = myController.getCurrentUser();
		
		myController.login("Mike");
		final User mike = myController.getCurrentUser();
		
		myController.login("Dain");
		final User dain = myController.getCurrentUser();
		
		final Conference conf = new Conference("A Conference", mike, 
				new GregorianCalendar(2015, 1, 1).getTime(), new GregorianCalendar(2015, 2, 1).getTime());
		
		myPaper1 = new Paper(conf, jacob, "Test paper", new File("paper1"));
		myPaper2 = new Paper(conf, jacob, "Test paper Also", new File("paper2"));
		myPaper3 = new Paper(conf, jacob, "Test paper Again", new File("paper3"));
		
		myController.chooseConference(conf);
		conf.submitPaper(myPaper1);
		conf.submitPaper(myPaper2);
		conf.submitPaper(myPaper3);
		
		mike.addToReview(myPaper1);
		mike.addToReview(myPaper2);
		
		dain.addToReview(myPaper1);
	}

	/**
	 * Testing get review method returning two reviewers.
	 * 
	 * @author Jacob Trimble
	 * 
	 * Fails if 2 users are not found.
	 * Passes if 2 users are found.
	 */
	@Test
	public void testTwoReviewers() {
		final List<User> papers = myController.getReviewers(myPaper1);
		assertTrue("Two reviewers for paper 1.", papers != null && papers.size() == 2);
	}
	/**
	 * Testing get review method returning zero reviewers.
	 * 
	 * @author Jacob Trimble
	 * 
	 * Fails if 0 users are not found.
	 * Passes if 0 users are found.
	 */
	@Test
	public void testZeroReviewers() {
		final List<User> papers = myController.getReviewers(myPaper3);
		assertTrue("Zero reviewers for paper 3.", papers != null && papers.size() == 0);
	}
}

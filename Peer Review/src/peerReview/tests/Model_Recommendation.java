package peerReview.tests;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.Recommendation;
import peerReview.model.SubProgramChair;
import peerReview.model.User;

/**
 * Test all methods of the Recommendation Class
 * 
 * @author Dain Landis
 */
public class Model_Recommendation {

	/** a Recommendation */
	private Recommendation myRecommendation;	
	/** Contains a conference for testing. */
	private Conference myConference;
	
	/**
	 * Setup for Recommendation constructor
	 * 
	 * @author Dain Landis
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {	
		myConference = new Conference("Test", new User("Dain"), null, null);
		myRecommendation = new Recommendation(new SubProgramChair(new User("Mr Subprogram")), 
				new Paper(myConference, new User("Mr Author"), "The Authors Title", new File("TheAuthorPaper.pdf")), 
				new File("TheRecommendation.pdf"));
	}

	/**
	 * Testing the recommendation constructor for valid and invalid data
	 * Passes if each declaration matches against class getters.
	 * 
	 * @author Dain Landis
	 */
	@Test
	public void testValidRecommendationConstructor() {
		assertTrue("Error verifying valid recommendation file", 
				myRecommendation.getFileName().toString().equals("TheRecommendation.pdf"));
		assertTrue("Error verifying valid recommendation paper author", 
				myRecommendation.getPaper().getAuthor().getName().toString().equals("Mr Author"));
		assertTrue("Error verifying valid recommendation paper title", 
				myRecommendation.getPaper().getTitle().toString().equals("The Authors Title"));
		assertTrue("Error verifying valid recommendation paper file name", 
				myRecommendation.getPaper().getFileName().toString().equals("TheAuthorPaper.pdf"));
		assertTrue("Error verifying valid recommendation subPc user", 
				myRecommendation.getSubProgramChair().getUser().getName().toString().equals("Mr Subprogram"));
		
		assertFalse("Error verifying invalid recommendation file", 
				myRecommendation.getFileName().toString().equals("TheBADRecommendation.pdf"));
		assertFalse("Error verifying invalid recommendation paper author", 
				myRecommendation.getPaper().getAuthor().getName().toString().equals("Mr BAD Author"));
		assertFalse("Error verifying invalid recommendation paper title", 
				myRecommendation.getPaper().getTitle().toString().equals("The BAD Authors Title"));
		assertFalse("Error verifying invalid recommendation paper file name", 
				myRecommendation.getPaper().getFileName().toString().equals("TheBADAuthorPaper.pdf"));
		assertFalse("Error verifying invalid recommendation subPc user", 
				myRecommendation.getSubProgramChair().getUser().getName().toString().equals("Mr BAD Subprogram"));
	}

	/**
	 * Tests that the equals method works.
	 * 
	 * @author Jacob Trimble
	 */
	@Test
	public void testEquals() {
		final Recommendation equals = new Recommendation(
				new SubProgramChair(new User("Mr Subprogram")), 
				new Paper(myConference, new User("Mr Author"), "The Authors Title", new File("TheAuthorPaper.pdf")), 
				new File("TheRecommendation.pdf"));
		final Recommendation notEqual = new Recommendation(
				new SubProgramChair(new User("Mr Subprogram")), 
				new Paper(myConference, new User("Mr Author"), "The Authors Title", new File("TheAuthorPaper.pdf")), 
				new File("TheRecommendation2.pdf"));
		
		assertEquals("Recommendations are supposed to be equal.", equals, myRecommendation);
		assertNotEquals("Recommendations are not supposed to be equal.", notEqual, myRecommendation);
	}
}
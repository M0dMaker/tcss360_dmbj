/*
 * Peer Review
 * 
 * Team DMBj
 * TCSS 360
 */

package peerReview.tests;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;

import peerReview.controller.MainController;
import peerReview.model.Conference;
import peerReview.model.Paper;
import peerReview.model.SubProgramChair;
import peerReview.model.User;

/**
 * Tests the addPaperToSubPc method on the main controller.
 * 
 * @author Jacob Trimble
 */
public final class MainController_AddPaperToSubPC {

	/** Controller for testing. */
	private MainController myController;
	/** Contains the conference for testing. */
	private Conference myConference;
	/** An example paper. */
	private Paper myPaper;

	@Before
	public void setUp() {
		myController = new MainController();
		
		myController.login("Mike");
		final User mike = myController.getCurrentUser();
				
		myController.login("Jacob");
		final User jacob = myController.getCurrentUser();
		
		myController.login("Dain");
		final User dain = myController.getCurrentUser();
		
		myConference = new Conference("A Conference", mike, 
				new GregorianCalendar(2015, 1, 1).getTime(), new GregorianCalendar(2015, 2, 1).getTime());
		
		myPaper = new Paper(myConference, jacob, "Test paper", new File("paper"));
		
		myController.chooseConference(myConference);
		myConference.submitPaper(myPaper);
		
		myConference.addSubProgramChair(new SubProgramChair(jacob));
		myConference.addSubProgramChair(new SubProgramChair(dain));
	}

	/**
	 * Testing addPaperToSubPc method that is valid.
	 * 
	 * @author Jacob Trimble
	 * 
	 * Fails if the subPC is the author or if the subPC is assigned 4 papers.
	 * Passes if this is a valid submission.
	 */
	@Test
	public void testValid() {
		myController.login("Dain");
		final User dain = myController.getCurrentUser();
		
		final int ret = myController.addPaperToSubPC(myPaper, new SubProgramChair(dain));
		assertTrue("A bad paper for valid.", ret == 0);
	}

	/**
	 * Testing addPaperToSubPc method that is authoring his own.
	 * 
	 * @author Jacob Trimble
	 * 
	 * Fails if this is valid.
	 * Passes if the function returns a 1.
	 */
	@Test
	public void testAuthorTheirOwn() {
		myController.login("Jacob");
		final User jacob = myController.getCurrentUser();
		
		final int ret = myController.addPaperToSubPC(myPaper, new SubProgramChair(jacob));
		assertTrue("An author must not review their own.", ret == 1);
	}
	
	/**
	 * Testing addPaperToSubPc method that is given too many.
	 * 
	 * @author Jacob Trimble
	 * 
	 * Fails if this is valid.
	 * Passes if the function returns a 2.
	 */
	@Test
	public void testTooManyPapers() {
		myController.login("Jacob");
		final User jacob = myController.getCurrentUser();	
		
		myController.login("Dain");
		final User dain = myController.getCurrentUser();
		final SubProgramChair subPC = new SubProgramChair(dain);
		
		final Paper paper1 = new Paper(myConference, jacob, "Test paper Also", new File("paper1"));
		final Paper paper2 = new Paper(myConference, jacob, "Test paper Again", new File("paper2"));
		final Paper paper3 = new Paper(myConference, jacob, "Test paper Again!", new File("paper3"));
		final Paper paper4 = new Paper(myConference, jacob, "Bad name", new File("paper4"));

		assertTrue("Error submitting paper1.", 0 == myController.addPaperToSubPC(paper1, subPC));
		assertTrue("Error submitting paper2.", 0 == myController.addPaperToSubPC(paper2, subPC));
		assertTrue("Error submitting paper3.", 0 == myController.addPaperToSubPC(paper3, subPC));
		assertTrue("Error submitting paper3.", 0 == myController.addPaperToSubPC(paper4, subPC));
		
		final int ret = myController.addPaperToSubPC(myPaper, subPC);
		assertTrue("Cannot submit more than 4 papers.", ret == 2);
	}
	
	
}

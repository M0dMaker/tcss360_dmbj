# Peer Review - Team DMBj #

This is a school project for TCSS 360.  The goal of the project is to manage conference details like the submitted papers.  This can manage multiple conferences and papers.  Papers can be submitted to the conference and then reviewed.

This is written in Java using two files for back-end storage.  This assumes that it is used by a single person at a time.